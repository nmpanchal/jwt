<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ta_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('login_email');
            $table->string('login_password');
            $table->string('prefer_name');
            $table->string('activation_token');
            $table->string('api_secret');
            $table->string('job_title');
            $table->string('domain_name');
            $table->string('database_name');
            $table->string('phone');
            $table->boolean('is_verified');
            $table->boolean('is_admin')->default(true);
            $table->boolean('is_archived')->default(false);
            $table->smallInteger('last_online');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ta_users');
    }
}
