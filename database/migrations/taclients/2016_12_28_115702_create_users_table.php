<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
           $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('initial_password');
            $table->string('about_me')->nullable();
            $table->integer('department')->unsigned();
            $table->string('profileImage')->nullable();
            $table->string('auth_token')->nullable();
            $table->tinyinteger('device_type')->nullable();
            $table->string('device_token')->nullable();
            $table->timestamps();
        });
        /*Schema::table('users', function($table) {
          $table->engine = 'InnoDB';
          $table->foreign('department')->references('department_id')->on('departments');
        });*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
