<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('content', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('user_id');
          $table->tinyInteger('approval');
          $table->tinyInteger('approval_flag');
          $table->integer('department_id');
          $table->text('description');
          $table->string('image_url');
          $table->string('title');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('content');
    }
}
