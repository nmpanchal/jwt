<?php
namespace App\Transformers;

use App\Model\Clients\Setting;
use League\Fractal;

class SettingTransformer extends Fractal\TransformerAbstract
{

	/**
	 * @param Setting $Setting
	 * @return array
	 */
	public function transform(Setting $setting)
	{
		return [
			'id'      		=> (int) $setting->id,
			'type'   	=> $setting->type,
			'value'   	=> $setting->value,
		];
	}

	
}