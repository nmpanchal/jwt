<?php

namespace App\Transformers\Clients;

use App\Model\Clients\ApprovalType;
use League\Fractal\TransformerAbstract;

class ApprovalTypeTransformer extends TransformerAbstract
{
    /**
     * Related models to include in this transformation.
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * Turn this item object into a generic array.
     *
     * @param ApprovalType $approval_type
     * @return array
     */
    public function transform(ApprovalType $approvalType)
    {
        return [
            // attributes
            'value' => $approvalType->value
        ];
    }
}
