<?php

namespace App\Transformers\Clients;

use App\Model\Clients\Content;
use App\Model\Clients\ApprovalType;
use League\Fractal\TransformerAbstract;

class ContentTransformer extends TransformerAbstract
{

    /**
     * Related models to include in this transformation.
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    protected $defaultIncludes = [
        //'department',
        'approvalType'
    ];

    /**
     * Turn this item object into a generic array.
     *
     * @param ContentTransformer $content
     * @return array
     */
    public function transform(Content $content)
    {
        return [
            // attributes
            'id' => $content->id,
            'title' => $content->title,
            'description' => $content->description,
            'image' => $content->image_url,
            'approved' => (boolean)$content->approval,
            //'approval_type' => $content->approvalType,

            // links
          /*  'links' => [
                [
                    'rel' => 'self',
                    'href' => url('content'),
                ],
            ] */
        ];
    }


    /**
     * Include Department
     *
     * @return League\Fractal\ItemResource
     */
    public function includeDepartment(Content $content)
    {
        $department = $content->department_id;

        return $this->item($department, new DepartmentTransformer);
    }

    /**
     * Include ContentOwner
     *
     * @return League\Fractal\ItemResource
     */
    public function includeContentOwner(Content $content)
    {
        $contentOwner = $content->user_id;

        return $this->item($contentOwner, new ClientUserTransformer);
    }

    /**
     * Include approvalType
     *
     * @return League\Fractal\Collection
     */
    public function includeApprovalType(Content $content)
    {
      // use the relationship to fetch the linkedin Transformer
        return $this->item($content->approvalType, new ApprovalTypeTransformer);
    }


    /**
    * Include Tags hooked up with content
    *
    * @return League\Fractal\Collection
    **/


}
