<?php
namespace App\Transformers;

use App\Model\Book;
use App\Model\Job;
use App\Model\User;
use League\Fractal;

class UserTransformer extends Fractal\TransformerAbstract
{
	/**
	 * Note: do not expose any passwords!
	 * @param User $user
	 * @return array
	 */
	public function transform(User $user)
	{
		return [
			'id'      		=> (int) $user->userId,
			'first_name'   	=> $user->firstName,
			'last_name'   	=> $user->lastName,
			'email'   		=> $user->email,
			'phone'			=> $user->phone,
			'user_type'		=> $user->userType,
		];
	}


	/**
	 * Get client
	 * @param User $user
	 * @return Fractal\Resource\Item
	 */
	public function includeClient(User $user)
	{
		$client = $user->client;

		return $this->item($client, new ClientTransformer);
	}


}