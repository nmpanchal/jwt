<?php
namespace App\Transformers;

use App\Model\Book;
use App\Model\ContractType;
use App\Model\Job;
use App\Model\Sector;
use App\Model\User;
use League\Fractal;

class ContractTypeTransformer extends Fractal\TransformerAbstract
{

	public function transform(ContractType $contract_type)
	{
		return [
			'id'      		=> (int) $contract_type->jobTypeId,
			'title'   		=> $contract_type->title
		];
	}
}