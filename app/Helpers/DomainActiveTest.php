<?php
namespace App\Helpers;
use HttpClient;
use EllipseSynergie\ApiResponse\Contracts\Response;

class DomainActiveTest
{
    static function checkIfDomainIsActive($domain)
    {
      $client = new HttpClient();
      $res = $client->get('https://api.mailtest.in/v1/'.$domain);
      $domainArr=json_decode($res->getBody()->getContents());
      return $domainArr->status;
    }
}
