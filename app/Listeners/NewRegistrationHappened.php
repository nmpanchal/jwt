<?php

namespace App\Listeners;

use App\Events\ClientVerified;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;
use App\Mail\NotifyAdmin;

class NewRegistrationHappened
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ClientVerified  $event
     * @return void
     */
    public function handle(ClientVerified $event)
    {
        $clientDetails['email'] = $event->user->login_email;
        $clientDetails['preferName'] = $event->user->prefer_name;
        Mail::to('ravi@talentadvocate.com')->send(new NotifyAdmin($clientDetails));
    }
}
