<?php

namespace App\Listeners;

use App\Events\ClientSignedup;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notifications\VerifyAccount;
use Illuminate\Notifications\Notifiable;
use App\User;
use Mail;
use App\Mail\SendVerificationEmail;

class NewClientRequest
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ClientSignedup  $event
     * @return void
     */
    public function handle(ClientSignedup $event)
    {
        //$data
        $user = User::find($event->client->id);

        $user1 =User::find(49);

        try{
          //prepare the mail data to be rendered via template
          $token = $user->activation_token;
          $send_url = 'e='.base64_encode($user->login_email).'&t='.base64_encode($token);
          $introLines[0] = 'We have received a sign up request for our platform using this email address.';
          $introLines[1] = 'We just need a final last click to confirm this was you.';
          $outroLines[0] = 'Thanks, we are looking forward to changing your world!!';

          $clientDetails = ['email'=>$user->login_email,
                            'greeting' => 'Hello, '.$user->prefer_name .'!',
                            'actionText' =>'Yes, confirm it!',
                            'actionUrl' => url('api/v1/verify?').$send_url,
                            'level' => 'success',
                            'introLines' => $introLines,
                            'outroLines' => $outroLines,
                            'footerText' => "Regards, Team Talentadvocate",
                            'headerText' => 'Welcome to the world of employee advocacy for Talent Acquisition'
                            ];
          //  Send the mail through mailable now
          Mail::to($user->login_email)->send(new SendVerificationEmail($clientDetails));
          // TODO: Analytics
          // @mention Ravi
          // Probably need to notify realtime to some action for analytics (A/B testing)

        } catch(\Exception $e){
          return "error : " .$e->getMessage();
        }

    }
}
