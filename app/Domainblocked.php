<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Domainblocked extends Model
{
    protected $connection ='mysql';
    protected $table = 'ta_domainblocked';
    protected $fillable = ['id','domain','is_deleted'];
    
}
