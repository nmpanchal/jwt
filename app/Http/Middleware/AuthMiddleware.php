<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Input;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use App\Model\Clients\TenantClientUsers;


class CmsMiddleware
{
    /**
     * Sets the connection's database to the current CMS
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      $slug = $request->id;

      $user = User::where('slug','=',$slug)->first();

      if(!$user){
				return response('Unauthorized.', 401);
			}

      DB::disconnect('tenantsql');
      Config::set('database.connections.tenantsql.database', $user->database_name);
      $conn = DB::connection('tenantsql');

      return $next($request);
    }
}
