<?php
namespace App\Http\Middleware;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use App\User;

class SetClientDatabase {
	/**
	 * Sets the connection's database to the current user database
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
	    // Get the current authenticated user
	    //$user = Auth::user();

			// TODO: Make a custom Auth LATER
			// @mention Ravi

			$user = User::where('api_secret','=',$request->get('api_secret'))
								->where('is_archived','=',0)
								->first();
			if(!$user){
				return response('Unauthorized.', 401);
			}
	    // Close any connection made before to avoid conflicts
	    DB::disconnect('tenantsql');
	    // Set the connection's database to the user's own database
	    Config::set('database.connections.tenantsql.database', $user->database_name);
			//dd($user->database_name);
			$conn = DB::connection('tenantsql');
			try{
				$conn->getPdo();
			} catch (\Exception $e) {
				return response('Not a valid secret, account inactive or Database gone?.', 401);
			}
    return $next($request);
	}
}
