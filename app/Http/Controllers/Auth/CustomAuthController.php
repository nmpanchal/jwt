<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Clients\TenantClientUsers;
use Illuminate\Support\Facades\Hash;

class CustomAuthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function issueToken(Request $request)
    {
        $input  = $request->all();

        $data = TenantClientUsers::where('email' , $request->email)->first();

        if($data){
            if(Hash::check($request->password, $data->password)){
                $tokenStr = str_random(10);
                $token = base64_encode($tokenStr);
                $data->auth_token = $tokenStr;
                $details = TenantClientUsers::find($data->id);
                $data->save();
                return response()->json(compact('token','data'));
            }else{
                return response()->json(['error' => 'username and password do not match'], 401);
            }
        }else{
            return response()->json(['error' => 'user not found'], 401);
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function Login(Request $request)
    {
        $input  = $request->all();

        $data = TenantClientUsers::where('email' , $request->email)->first();

        if($data){
            if(Hash::check($request->password, $data->password)){
                $token = base64_encode(str_random(10));
                $data->auth_token = $token;
                $data->save();
                return response()->json(compact('token'));
            }else{
                return response()->json(['error' => 'username and password do not match'], 401);
            }
        }else{
            return response()->json(['error' => 'user not found'], 401);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
