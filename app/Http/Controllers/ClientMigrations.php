<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class ClientMigrations extends Controller
{

  /**
   * Migrate default system tables in to client Database
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   * @auther Nilesh Panchal <nilesh@isensical.com>
   * @review Ravi Tiwari <ravi@talentadvocate.com>
   */
  public function generateClientTables($databaseName)
  {
     DB::disconnect('tenantsql');
    // Set the connection's database to the user's own database
    Config::set('database.connections.tenantsql.database', $databaseName);
     //DB::beginTransaction();

     try{
       \Artisan::call('migrate', array('--path' => '/database/migrations/taclients', '--database'=> 'tenantsql', '--force' => true));
      // dd($migrationResponse);
       $content = array("success", "Platform is ready to use now.");

     } catch(\Exception $e) {
         //DB::rollBack();
         $content = array("error","Something went wrong with system, we will revert shortly?".$e->getMessage());
     } finally{
       //DB::disconnect('tenantsql');
       //DB::connect('mysql');
     }
    // DB::commit();
     return $content;

  }

  public function createClientSchema($schemaName){
    //dd('#schema Create Call');
    $conn = DB::connection('mysql');
    $strStatement = "CREATE DATABASE IF NOT EXISTS `".$schemaName."` DEFAULT CHARACTER SET = 'utf8' DEFAULT COLLATE 'utf8_unicode_ci'";//str_replace('.','_',$client->database_name);
    $p = $conn->statement($strStatement);
    return $p;

  }


}
