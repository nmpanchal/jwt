<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class CustomLoginController extends Controller
{
  /**
   * Authorise user
   * @param email
   * @param password
   * @auther Nilesh Panchal
   * @created at 2nd January,2017
   * @return mixed
   **/

   public function Login(Request $request){
     dd('hola');
     // TODO: right now api token being assumed to be correct by default, if wrong email/pass/hint will come up
     // Later this can be moved into Requests/validateLogin.php for Validation (service orchestration)
     $rules = array(
          'email' => 'required|email',
          'password' => 'required|min:6',
      );
      $validator = Validator::make($request->json()->all(),$rules);
      if($validator->fails()){
        // due to reused routes for call forwarding we need to verify token too
        // if its FWD route and validator failed means api token being wrong
        ($request->method()=='GET')?$resData['error'] = 'Verify token or request type !':$resData = array('error'=>$validator->errors());
      } else {
        $emailUser = $request->json()->get('email');
        $passUser = $request->json()->get('password');
        // get domain name and hookup the client Table
        $domainOnly = preg_replace('/.+@/', '', $emailUser);
        $domainExists =$this->userRepository->getByDomain($domainOnly);
        // if domain exists connect to client DB and authenticate
        if(isset($domainExists['error']) && $domainExists['error']){
          return response()->json($domainExists, 201);
        }

        // route Forwarding for re-use
        return redirect()->route('profile')->with(
                [
                  'api_secret' => $domainExists['api_secret'],
                  'email'=>$emailUser,
                  'password' =>$passUser,
                  'type' => 'redirect',
                  ]
                );

      }
      return response()->json($resData, 201);
   }
}
