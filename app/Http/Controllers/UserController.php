<?php

namespace App\Http\Controllers;

use App\Model\User;
use DB;
use App\Domainblocked;
use App\User as TAUSER;
use App\Events\ClientSignedup as EvSignedup;
use Illuminate\Http\Request;
use DomainActiveHelper;
//use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Validation\ValidatesRequests;
use EllipseSynergie\ApiResponse\Contracts\Response;
use App\Notifications\ClientSignedup;
use Illuminate\Notifications\Notifiable;
use App\Http\Controllers\ClientMigrations;
use Illuminate\Support\Facades\Config;
use App\Model\Clients\TenantClientUsers;

class UserController extends Controller
{

    public function __construct(Response $response, Request $request, UserRepository $userRepository){
      $this->request = $request;
      $this->response = $response;
      $this->userRepository = $userRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $reqData =$this->request->json()->all();

        $rules = array(
            'fname' => 'required|min:2',
            'lname' => 'required',
            'email' =>'unique:ta_users,login_email',  // mapping with custom coulmn name field
            'job' => 'required',
            'phone' => 'required',
            'company' => 'required'
        );

        $validator = Validator::make($reqData,$rules);
        if($validator->fails()){
          return \Response::json(array('error' =>$validator->errors()), 422);
        }
        //all fine lets check generic email domain which are not allowed
        $domainOnly = preg_replace('/.+@/', '', $reqData['email']);

        /*
        // ***********
        // TODO LIVE: enable when go live/ TEST
        // Ask helper to check out if domain spoofing present
        $status = DomainActiveHelper::checkIfDomainIsActive($domainOnly);
        if($status !== 'ACTIVE'){
          return \Response::json(array('error' =>array('domain'=>'Given mail domain is not a valid domain. If you do not agree hitback hi@talentadvocate.com')), 422);
        }
        // *********
        */
        // its real domain, fine check for prohibited by TalentAdvocate
        if(Domainblocked::where('domain','=',$domainOnly)->count()){
          return \Response::json(array('error' =>array('domain'=>'Use your corporate email id, given mail domain is prohibited for registration.')), 422);
        }

        // Insert into ta_users now as its all fine
        try{
        DB::beginTransaction();
        $status = 201;
        $client =User::firstOrCreate(
              ['prefer_name' => $reqData['fname'].' '.$reqData['lname'],
              'login_email' => $reqData['email'],
              'job_title' => $reqData['job'],
              'phone' => $reqData['phone'],
              'domain_name' => $domainOnly,
              'database_name' => str_replace('.','_',$domainOnly),
              'is_admin'=> true,
              'login_password' => bcrypt($reqData['fname'].$domainOnly),
              'api_secret' => str_random(10),
              'activation_token' => str_random(15)
            ]);
            //dd($client);
        // After successful insert fire Event : ClientSignedup
        // TAUSER::find(19)->notify(new ClientSignedup($client)); //Trick to rollup not used thou
        event(new EvSignedup($client));
      } catch(\Exception $e) {
          $status = 500;
					$client = array("error"=>array( 'DB' =>"Something went wrong with insert, probably query?".$e->getMessage()));
          DB::rollback();
      }
			DB::commit();
      return \Response::json($client, $status);
    }



    /**
     * Verify a newly created client.
     *
     * @param  $request
     * @return \Illuminate\Http\Response
     */
    public function verify()
    {
        $email = base64_decode($this->request->get('e'));
        $acToken = base64_decode($this->request->get('t'));
        if($email && $acToken){
          $client = User::where('login_email','=', $email)
                        ->where('activation_token','=', $acToken)
                        ->where('is_verified','=',0)
                        ->first();
          if($client){
            return  \Response::json($this->update($client), 200);
          }
        }
          return \Response::json(array("error"=>array('Invalid request' =>'Link has been expired or its invalid')), 422);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($client)
    {
        try{
        //$client->is_verified = true;
        //$client->save();
        // Now run migrations automated
        $migrationClient = new ClientMigrations;
        $migrationClient->createClientSchema($client->database_name);


        $feedback = $migrationClient->generateClientTables($client->database_name);
        //dd($feedback);
        if($feedback[0]=="success"){
          $rndPass = str_random(10);
          $newClientUser = TenantClientUsers::firstOrCreate(
                ['name' => $client->prefer_name,
                 'password' =>bcrypt($rndPass),
                 'initial_password' =>$rndPass,
                 'about_me' => "TalentAdvocate",
                'email' => $client->login_email,
                'department' => 1, // TODO: default will be HR/Admin
                'auth_token' => str_random(10)
              ]);
          // Account verified now set verified
          $client->is_verified=1;
          $client->is_admin=1;
          $client->save();
        }
        $feedback1 = array($feedback[0]=> $feedback[1] . '. Your account has been verified successfully !');
      } catch (\Exception $e){
        $feedback1 = array("error" => array('Server Issue'=>'Contact hi@talentadvocate.com as server made some boo boo!!'.$e->getMessage()));
      }
      return $feedback1;
    }


}
