<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use App\Repositories\Clients\ClientUserRepository;
use Illuminate\Support\Facades\Validator;
use App\Model\Clients\TenantClientUsers as ClientUser;
use DB;
use App\Model\OauthClient as OauthClient;

class ClientController extends BaseController {

  public function __construct(Response $response, Request $request, ClientUserRepository $clientRepository){
//  public function __construct(Response $response, Request $request){
    //parent::_construct();
    $this->request = $request;
    $this->response = $response;
    $this->clientRepository = $clientRepository;
  }

  public function index(){

    // if this is first login (flag set or not)
    // do a walkthough with settings and run respective
    // artisan commands in steps
    // TODO : its bad way doing auth call multiple times think Session
    $user = Auth::user();
    if(!$user->completedSetup && $user->isAdmin ){
      // Welcome view with steps
      // do step by step settings page walkthrough


    } else {
      // show dashboard view

    }

  }

  public function storeGenericSettings(Request $request){
    // Store country, sector, job & application states
    $req = $request::all();

  }


  public function setupClient(){
    // Artisan call migrate all required SCHEMA
    // Run migrations for the new clientDB
    try {
      Artisan::call('migrate', ['--database' => 'tenantsql', '--path' => 'database/migrations/taclients']);
      } catch(Exception $e){
        abort(404, 'Something went wrong !! Please contact administrator');
      }
  }


  public function storeBusinessRules(Request $request){
    $req = $request::all();

  }

  /**
   * Stores client detail in database and send an followup email for verification
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   * @auther Nilesh Panchal <nilesh@isensical.com>
   */
  public function store(Request $request){

    $req = $request->all();
    $response = array();
    $rules = array(
              'name'       => 'required',
              'email'      => 'required|email',

          );
          $validator = Validator::make(Input::all(), $rules);

          // process the login
          if ($validator->fails()) {
                  $response['error'] = $validator;
          } else {
              // store
              $user = new User;
              $user->name       = Input::get('name');
              $user->email      = Input::get('email');
              $user->save();

              $response['message'] = 'success';
          }
  }

  public function createClientUser(Request  $request){
          //$dbToken =  $request->input('api_token');
          $name     = $request->input('name');
          $email    = $request->input('email');
          $department = $request->json()->get('department');
          $password = str_random(10);

          //step:1  first save user in client databse
          try {
            $clientUser = ClientUser::firstOrCreate(
                  ['name' => $name,
                   'password' =>bcrypt($password),
                   'initial_password' =>$password,
                   'about_me' => "TalentAdvocate",
                   'email' => $email,
                   'department' => 1, // TODO: default will be HR/Admin
                   'auth_token' => str_random(10)
                ]);
          } catch (Exception $e) {
              return $e;
          }
          //dd('fine');
          //step:2 create oauth client
          $oauth_client = OauthClient::create([
              'user_id'                => $clientUser->id,
              'id'                     => $email,
              'name'                   => $name,
              'secret'                 => base64_encode(hash_hmac('sha256',$password, 'secret', true)),
              'password_client'        => 1,
              'personal_access_client' => 0,
              'redirect'               => '',
              'revoked'                => 0
          ]);

          $response = ['message' => 'user successfully created.'];
          return response()->json($response);

  }

}
