<?php

namespace App\Http\Controllers\Clients;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\SettingRepositoryInterface;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Validator;
use App\Transformers\SettingTransformer;
use EllipseSynergie\ApiResponse\Contracts\Response;


class SettingController extends BaseController
{
    public function __construct(Request $request, Response $response, SettingRepositoryInterface $settingRepository){
        $this->request = $request;
        $this->settingRepository = $settingRepository;
        $this->response = $response;
    }

    /**
    *Add setting
    *@author Yamin
    *@param Type,Value
    *@return Json
    *@link  http://192.168.99.100:9000/api/v1/setting/add?api_secret=PfxeESfZhp
    {
		"type": "default Tags",
		"value": "CEO calls"
	}
    **/
    public function create(){
    	$inputJSON = $this->request->json()->all();
    	$v = Validator::make($this->request->json()->all(), [
		        'type' => 'required|max:255',
		        'value' => 'required|max:255',
        ]);

        if(!$inputJSON){
		      return \Response::json(["code" => 400, "Error" =>" Error with Input", "message"=>"Json is invalid" ]);
		} elseif($v->fails()){
		      return \Response::json(["code" => 400, "Error" =>" Error with Input", "message"=>$v->errors()]);
		}

		$settingId = $this->settingRepository->addSetting($inputJSON);
		if(!is_integer($settingId)){
			return \Response::json(["data" => NULL, "message"=>"Problem in adding setting"], 404);
		}
		return \Response::json(["data" => json_encode($settingId), "message"=>"setting added successfully"], 200);
    }

    /**
    *Edit setting
    *@author Yamin
    *@param Type,Value,id
    *@return Json
    *@link  http://192.168.99.100:9000/api/v1/setting/edit?api_secret=PfxeESfZhp
    {
		"type": "default Tags",
		"value": "HR Announcment",
		"id": 1
	}
    **/

    public function edit(){
    	$inputJSON = $this->request->json()->all();
    	$v = Validator::make($this->request->json()->all(), [
		        'type' => 'required|max:255',
		        'value' => 'required|max:255',
		        'id' => 'required|integer',
        ]);

        if(!$inputJSON){
		      return \Response::json(["code" => 400, "Error" =>" Error with Input", "message"=>"Json is invalid" ]);
		} elseif($v->fails()){
		      return \Response::json(["code" => 400, "Error" =>" Error with Input", "message"=>$v->errors()]);
		}

		$editSuccess = $this->settingRepository->editSetting($inputJSON);
		if(!$editSuccess){
			return \Response::json(["data" => NULL, "message"=>"Problem in updating setting"], 404);
		}
		return \Response::json(["data" => json_encode($inputJSON['id']), "message"=>"setting updated successfully"], 200);

    }

    /**
    *List setting with filter (all or with type)
    *@author Yamin
    *@param Type
    *@return Json
    *@link  http://192.168.99.100:9000/api/v1/setting?api_secret=PfxeESfZhp
    {
		"type": "default Tags",
	}
    **/

    public function show(){
    	$inputJSON = $this->request->json()->all();
    	$v = Validator::make($this->request->json()->all(), [
		        'type' => 'required|max:255',
        ]);

        if(!$inputJSON){
		      return \Response::json(["code" => 400, "Error" =>" Error with Input", "message"=>"Json is invalid" ]);
		} elseif($v->fails()){
		      return \Response::json(["code" => 400, "Error" =>" Error with Input", "message"=>$v->errors()]);
		}

		$settingData = $this->settingRepository->listSetting($inputJSON);
		if(count($settingData) == 0){
			return \Response::json(["data" => NULL, "message"=>"No setting found"], 404);
		}
		return $this->response->withCollection(
                $settingData,
                new SettingTransformer
        );

    }


    /**
    *Remove setting by id
    *@author Yamin
    *@param Type
    *@return Json
    *@link  http://192.168.99.100:9000/api/v1/setting/delete?api_secret=PfxeESfZhp
    {
		"id": "2",
	}
    **/

    public function delete(){
    	$inputJSON = $this->request->json()->all();
    	$v = Validator::make($this->request->json()->all(), [
		        'id' => 'required|integer',
        ]);

        if(!$inputJSON){
		      return \Response::json(["code" => 400, "Error" =>" Error with Input", "message"=>"Json is invalid" ]);
		} elseif($v->fails()){
		      return \Response::json(["code" => 400, "Error" =>" Error with Input", "message"=>$v->errors()]);
		}

		$settingRemove = $this->settingRepository->removeSetting($inputJSON);
		if(!$settingRemove){
			return \Response::json(["data" => NULL, "message"=>"Problem in deleting setting"], 404);
		}
		return \Response::json(["data" => NULL, "message"=>"Setting deleted successfully"], 200);

    }
}
