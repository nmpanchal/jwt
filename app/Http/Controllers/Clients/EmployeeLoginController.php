<?php

namespace App\Http\Controllers\Clients;

use App\Model\Clients\TenantClientUsers as ClientUser;
use App\Model\Clients\UserSocial as SocialList;
use App\Model\Clients\SocialAccounts as SocialAccountsListed;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Validator;

class EmployeeLoginController extends ApiController
{
  public function __construct(Response $response, Request $request){
    //$this->middleware('database');
    $this->request = $request;
    $this->response = $response;
  }


  /**
   * Verify email & password used for auth
   * @param email
   * @param password
   * @return mixed
   * TODO: Review in Phase 2 for smaller services orchestration
   * like matchEmail, matchPassword, saveProfile
   **/

   public function profile(Request $request){
        // custom Auth manual can be made scalable for later
        // use ie. using a custom Middleware
        $email = $request->get('email');
        $pass = $request->get('password');
        $callType = $request->get('type');
        // this below can be used in atomic profile service
        $clientUser = ClientUser::where('email','=',$email)
                      ->with(array('department', 'socialAccounts'))
                      ->first();
        $resData = $clientUser;
        //dd(count($clientUser));
        // if valid match the password and confirm back with profile info
        if(count($clientUser) && $callType=='redirect'){
              // there is match for mail now match the password
              if(Hash::check($pass,$clientUser->pluck('password')[0])){
                // give api_secret back for successive requests
                $resData->api_secret = $request->get('api_secret');
                // append device token field for mobile devices

              } else{
                $resData = array('error' =>
                                array('Input' =>
                                array('Password invalid for the email provided !')));

              }
        }else{
              $resData = count($resData)? $resData : array('error' =>
                              array('Input' =>
                              array('Email missing or not registered, please contact your admin !')));

        }
        return response()->json($resData, 200);
   }

}
