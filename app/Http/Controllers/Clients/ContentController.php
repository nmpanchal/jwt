<?php

namespace App\Http\Controllers\Clients;

use Illuminate\Http\Request;
use App\Http\Requests\Clients\GetContentRequest;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Validator;
use App\Transformers\Clients\ContentTransformer;
use EllipseSynergie\ApiResponse\Contracts\Response;
use App\Repositories\Clients\ContentRepositoryEloquent;
use App\Repositories\Clients\ClientUserRepository;
use App\Model\Clients\ApprovalType as ApprovalType;
use App\Model\Clients\Content as Content;
use Vinkla\Hashids\Facades\Hashids;
use Route;
//use App\Talentadvocate\EloquentBuilderTrait;

class ContentController extends ApiController
{
  //use EloquentBuilderTrait;

    public function __construct(Response $response, Request $request,
        ClientUserRepository $clientuserRepository,
        ContentRepositoryEloquent $contentRepository){
      $this->request = $request;
      $this->response = $response;
      $this->clientuserRepository = $clientuserRepository;
      $this->contentRepository = $contentRepository;
    }

    public function index(Request $request){

      // TODO: a middleware to have this later on once auth is implemented
      $token  = base64_decode($request->get('auth_token'));

      $clientUser = $this->clientuserRepository->getByToken($token);

      if($clientUser){
        $department = $clientUser->department;
        $userId = array();
        $userId[] = $clientUser->id;

        //get all approved only
        $contents = $this->contentRepository
                          ->getAll($userId,$department, 1)->paginate(20);
        return $this->response->withPaginator(
              			$contents,
              			new ContentTransformer
              		);
      }else{
        return response()->json(['error' => 'unauthorised'], 401);
      }


    }

    public function getAllApproved(){
      // Parse the resource options given by GET parameters
      // $resourceOptions = $this->parseResourceOptions(); //TRAIT for filters/include

      // TODO: a middleware to have this later on once auth is implemented
      $clientUser = $this->clientuserRepository->getByToken('VY5FfBIIdcOFh');
      $department = $clientUser->department;
      $userId = array();
      $userId[] = $clientUser->id;

      // filter all approved with pagination with department
      // department is key with user's department switch possibility
      $contents = $this->contentRepository
                        ->getAll($userId,$department, 1)->paginate(20);
      return $this->response->withPaginator(
                  $contents,
                  new ContentTransformer
                );
    }

    public function getAllNotApproved(){
      // TODO: a middleware to have this later on once auth is implemented
      $clientUser = $this->clientuserRepository->getByToken('VY5FfBIIdcOFh');
      $department = $clientUser->department;
      $userId = array();
      $userId[] = $clientUser->id;

      //get all approved only
      $contents = $this->contentRepository
                        ->getAll($userId,$department, 0)->paginate(20);

      return $this->response->withPaginator(
                  $contents,
                  new ContentTransformer
                );
    }

    public function createContent(Request $request){
      $reqData =$request->json()->all();
      $rules = array(
          'title' => 'required|min:50',
          'description' => 'required',
          'media' =>'required',
          'motive_approval' => 'required'
      );

      $validator = Validator::make($reqData,$rules);
      if($validator->fails()){
        return \Response::json(array('error' =>$validator->errors()), 422);
      }

      // get default user who is creating the content
      // get department too of that user
      // TODO: a middleware to have this later on once auth is implemented
      $clientUser = $this->clientuserRepository->getByToken('VY5FfBIIdcOFh');
      $department = $clientUser->department;
      $userId = array();
      $userId[] = $clientUser->id;

      $contentCreated = $this->contentRepository
                      ->create($userId[0],$department, $reqData);

      if(count($contentCreated['error'])){
        return $this->response->errorInternalError($contentCreated['error']);
      }
      //dd($contentCreated->id);
      return $this->response->withItem(
                  $contentCreated,
                  new ContentTransformer
                );

    }

    public function approveContent(Request $request){
      $reqData =$request->json()->all();
      // Identifier for content required here with authority of edit
      $rules = array(
        'id' => 'required',
        'approved' => 'boolean',
        'approval_type' => 'integer|between:1,3'
        );
      $validator = Validator::make($reqData,$rules);
      if($validator->fails()){
        return \Response::json(array('error' =>$validator->errors()), 422);
      }

      // Convert back to normal id from hashedKey
      $contentId = Hashids::decode($request->json()->get('id'));
      $contentupdated = $this->contentRepository
                      ->update($contentId, $reqData);

      if(count($contentupdated['error'])){
        return $this->response->errorInternalError($contentupdated['error']);
      }
      //dd($contentCreated->id);
      return $this->response->withItem(
                  $contentupdated,
                  new ContentTransformer
                );


    }



}
