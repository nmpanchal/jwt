<?php

namespace App\Http\Controllers\Clients;

use Illuminate\Http\Request;

use App\Model\Clients\Category;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;


use App\Http\Requests;

class CategoryController extends BaseController
{
    public function __construct(Response $response, Request $request){
        $this->request = $request;
        $this->response = $response;
    }
 
    /**
    *List all category (if you pass user id it will give you tags per user)
    *@author Yamin
    *@param Request (JSON)
    *@return user_id (sometime)
    *@link http://192.168.99.100:9000/api/v1/category?api_secret=PfxeESfZhp
    {
		"user_id": 16	
    }
    **/
    public function show(){
    	$inputJSON = $this->request->json()->all();
        $v = Validator::make($this->request->json()->all(), [
                  'user_id' => 'sometimes | required | integer',
        ]);

        if(!$inputJSON && count($inputJSON) != 0){
                return \Response::json(["code" => 400, "Error" =>" Error with Input", "message"=>"Json is invalid" ]);
        } elseif($v->fails()){
                return \Response::json(["code" => 400, "Error" =>" Error with Input", "message"=>$v->errors()]);
        }
        $user_id = $this->request->json()->get('user');

    	if($user_id != "" && $user_id != 0){
    		$category = Category::where('user_id',$user_id)->get();
    	}else{
    		$category = Category::all();	
    	}

    	if(count($category)==0){
    		return \Response::json(["data" => NULL, "message"=>"No category found"], 404);
    	}
    	return \Response::json(["data" => json_encode($category), "message"=>"category list"], 200);
    }

    /**
    *Create Category
    *@author Yamin
    *@param Request(JSON)
    *@return JSON
    *@link http://192.168.99.100:9000/api/v1/category/add?api_secret=PfxeESfZhp
    {
		"user_id": 16,
		"category": "Sales"
	}
    **/
    public function create(){
    	$inputJSON = $this->request->json()->all();
    	$v = Validator::make($this->request->json()->all(), [
		        'category' => 'required|max:255',
		        'user_id' => 'required|integer',
        ]);
        if(!$inputJSON){
		      return \Response::json(["code" => 400, "Error" =>" Error with Input", "message"=>"Json is invalid" ]);
		} elseif($v->fails()){
		      return \Response::json(["code" => 400, "Error" =>" Error with Input", "message"=>$v->errors()]);
		}
		$category = new Category();
		$categoryId = $category->addCategory($inputJSON);

		if(!is_integer($categoryId)){
			return \Response::json(["data" => NULL, "message"=>"Problem in adding category"], 404);
		}
		return \Response::json(["data" => json_encode($categoryId), "message"=>"category added successfully"], 200);	


    }

    /**
    *Edit Category
    *@author Yamin
    *@param Request(JSON)
    *@return JSON
    *@link http://192.168.99.100:9000/api/v1/category/edit?api_secret=PfxeESfZhp
    {
		"user_id": 1,
		"category": "Sales & team",
		"id": 4,
	}
    **/
    public function edit(){
    	$inputJSON = $this->request->json()->all();
    	$v = Validator::make($this->request->json()->all(), [
		        'category' => 'required|max:255',
		        'user_id' => 'required|integer',
		        'id' => 'required|integer'
        ]);
        if(!$inputJSON){
		      return \Response::json(["code" => 400, "Error" =>" Error with Input", "message"=>"Json is invalid" ]);
		} elseif($v->fails()){
		      return \Response::json(["code" => 400, "Error" =>" Error with Input", "message"=>$v->errors()]);
		}

		$category = new Category();
		$categoryEdit = $category->editCategory($inputJSON);

		if(!$categoryEdit){
			return \Response::json(["data" => $this->request->json()->get('id'), "message"=>"Problem in editing category"], 404);
		}
		return \Response::json(["data" => $this->request->json()->get('id'), "message"=>"Category edited successfully"], 200);	
    }

    /**
    *Remove Category
    *@author Yamin
    *@param Request(JSON)
    *@return JSON
    *@link http://192.168.99.100:9000/api/v1/category/delete?api_secret=PfxeESfZhp
    {
		"id": 5,
	}
    **/
    public function delete(){
    	$inputJSON = $this->request->json()->all();
    	$v = Validator::make($this->request->json()->all(), [
		        'id' => 'required|integer'
        ]);
        if(!$inputJSON){
		      return \Response::json(["code" => 400, "Error" =>" Error with Input", "message"=>"Json is invalid" ]);
		} elseif($v->fails()){
		      return \Response::json(["code" => 400, "Error" =>" Error with Input", "message"=>$v->errors()]);
		}
		$category = new Category();
		$categoryDelete = $category->deleteCategory($this->request->json()->get('id'));

		if(!$categoryDelete){
			return \Response::json(["data" => $this->request->json()->get('id'), "message"=>"Problem in deleting category"], 404);
		}
		return \Response::json(["data" => NULL, "message"=>"category deleted successfully"], 200);
    }

    /**
    *Filter category (for now type, may add more in future)
    *@author Yamin
    *@param Request (JSON)
    *@return JSON
    *@link http://192.168.99.100:9000/api/v1/category/filter?api_secret=9cXKYU4Wt8
    {
		"client_id": 1,
		"filter_type": "type",
		"filter_value": "job"
    }
    **/

    /*public function filter(Request $request){
    	$inputJSON = $request->json()->all();

    	if($request->json()->get('client_id') != "" && $request->json()->get('client_id') != 0){
    		$v = Validator::make($request->json()->all(), [
		          'client_id' => 'required | integer',
		          'filter_type' => 'required',
		          'filter_value' => 'required'
        		 ]);
    		if(!$inputJSON){
		        return \Response::json(["code" => 400, "Error" =>" Error with Input", "message"=>"Json is invalid" ]);
		    } elseif($v->fails()){
		        return \Response::json(["code" => 400, "Error" =>" Error with Input", "message"=>$v->errors()]);
		    }
		    $category = Category::where('client_id',$request->json()->get('client_id'))->where($request->json()->get('filter_type'),$request->json()->get('filter_value'))->get();
    	}else{
    		$v = Validator::make($request->json()->all(), [
		          'filter_type' => 'required',
		          'filter_value' => 'required'
        		 ]);
    		if(!$inputJSON){
		        return \Response::json(["code" => 400, "Error" =>" Error with Input", "message"=>"Json is invalid" ]);
		    } elseif($v->fails()){
		        return \Response::json(["code" => 400, "Error" =>" Error with Input", "message"=>$v->errors()]);
		    }
    		$category = Category::where($request->json()->get('filter_type'),$request->json()->get('filter_value'))->get();	
    	}

    	if(count($category)==0){
    		return \Response::json(["data" => NULL, "message"=>"No category found"], 404);
    	}
    	return \Response::json(["data" => json_encode($category), "message"=>"category filter list"], 200);
    }*/
}
