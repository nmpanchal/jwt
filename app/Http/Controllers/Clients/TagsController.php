<?php

namespace App\Http\Controllers\Clients;

use Illuminate\Http\Request;
use App\Model\Clients\Tags;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Config;



use App\Http\Requests;

class TagsController extends BaseController
{

    public function __construct(Response $response,Request $request)
    {
        $this->response = $response;
        $this->request = $request;

    }
     /**
        *GET: List all tags (if you pass user id it will give you tags per user)
        *@author Yamin
        *@param Request (JSON)
        *@return JSON    
        *@link http://192.168.99.100:9000/api/v1/tags?api_secret=PfxeESfZhp
        {
            "user_id": 16 
        }
     **/

    public function show(){
    	$inputJSON = $this->request->json()->all();
        if($this->request->json()->get('user_id') != "" && $this->request->json()->get('user_id') != 0){
            //only if user id available 
            $v = Validator::make($this->request->json()->all(), [
                  'user_id' => 'required | integer',
            ]);

            if(!$inputJSON){
                return \Response::json(["code" => 400, "Error" =>" Error with Input", "message"=>"Json is invalid" ]);
            } elseif($v->fails()){
                return \Response::json(["code" => 400, "Error" =>" Error with Input", "message"=>$v->errors()]);
            }
            $tags = Tags::where('user_id',$this->request->json()->get('user_id'))->get();
        }else{
            $tags = Tags::all();    
        }
        if(count($tags)==0){
            return \Response::json(["data" => NULL, "message"=>"No tags found"], 404);
        }
        return \Response::json(["data" => json_encode($tags), "message"=>"tags list"], 200);

    }

    /**
    *Create Tags
    *@author Yamin
    *@param Request(JSON)
    *@return JSON
    *@link http://192.168.99.100:9000/api/v1/tags/add?api_secret=PfxeESfZhp
    {
		"user_id": 1,
		"tag_name": " tag",
	}
    **/
    public function create(){
    	$inputJSON = $this->request->json()->all();
    	$v = Validator::make($this->request->json()->all(), [
		        'tag_name' => 'required|max:255',
		        'user_id' => 'required|integer'
        ]);

        if(!$inputJSON){
		      return \Response::json(["code" => 400, "Error" =>" Error with Input", "message"=>"Json is invalid" ]);
		} elseif($v->fails()){
		      return \Response::json(["code" => 400, "Error" =>" Error with Input", "message"=>$v->errors()]);
		}

		$tags = new Tags();
		$tagsId = $tags->addTag($this->request->json()->get('tag_name'),$this->request->json()->get('user_id'));

		if(!is_integer($tagsId)){
			return \Response::json(["data" => NULL, "message"=>"Problem in adding tag"], 404);
		}

		return \Response::json(["data" => json_encode($tagsId), "message"=>"Tag added successfully"], 200);	
    }


    /**
    *Edit Tags
    *@author Yamin
    *@param tag_name,user_id,id
    *@return JSON
    *@link http://192.168.99.100:9000/api/v1/tags/edit?api_secret=PfxeESfZhp
    {
		"user_id": 1,
		"tag_name": "edited tag",
		"id": 7
	}
    **/
    public function edit(){
    	$inputJSON = $this->request->json()->all();
    	$v = Validator::make($this->request->json()->all(), [
		        'tag_name' => 'required|max:255',
		        'user_id' => 'required|integer',
		        'id' => 'required|integer'
        ]);

        if(!$inputJSON){
		      return \Response::json(["code" => 400, "Error" =>" Error with Input", "message"=>"Json is invalid" ]);
		} elseif($v->fails()){
		      return \Response::json(["code" => 400, "Error" =>" Error with Input", "message"=>$v->errors()]);
		}

		$tags = new Tags();
		$tagsEdit = $tags->editTag($this->request->json()->get('tag_name'),$this->request->json()->get('user_id'),$this->request->json()->get('id'));
		if(!$tagsEdit){
			return \Response::json(["data" => $this->request->json()->get('id'), "message"=>"Problem in editing tag"], 404);
		}

		return \Response::json(["data" => $this->request->json()->get('id'), "message"=>"Tag edited successfully"], 200);
    }

    /**
    *Remove Tags
    *@author Yamin
    *@param Request(JSON)
    *@return JSON
    *@link http://192.168.99.100:9000/api/v1/tags/delete?api_secret=PfxeESfZhp
    {
		"id": 7,
	}
    **/
    public function delete(){
    	$inputJSON = $this->request->json()->all();
    	$v = Validator::make($this->request->json()->all(), [
		        'id' => 'required|integer'
        ]);
        if(!$inputJSON){
		      return \Response::json(["code" => 400, "Error" =>" Error with Input", "message"=>"Json is invalid" ]);
		} elseif($v->fails()){
		      return \Response::json(["code" => 400, "Error" =>" Error with Input", "message"=>$v->errors()]);
		}
		$tags = new Tags();
		$tagsDelete = $tags->deleteTag($this->request->json()->get('id'));

		if(!$tagsDelete){
			return \Response::json(["data" => $this->request->json()->get('id'), "message"=>"Problem in deleting tag"], 404);
		}
		return \Response::json(["data" => NULL, "message"=>"Tag deleted successfully"], 200);
    }


}
