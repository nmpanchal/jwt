<?php

namespace App\Repositories;
use App\Model\Job;
use App\Model\JobDetails;
use App\Model\PublishStatus;
use App\Model\ActivePeriod;
use Carbon\Carbon;

/**
 * Interface JobRepositoryInterface
 * @package App\Repositories
 */
Class JobRepository implements JobRepositoryInterface {

	protected $id;

	public function getById($jobid)
	{
		return Job::with('jobdetails')->find($jobid);
	}


	/**
	 * @param $filtername
	 *
	 */
	public function getByFilter($filtername , $filtervalue, $limit = NULL, $offset = NULL)
	{
    // This will be used for filtering job based on conditions
    $columnName = $filtername.'id';
		$resJob = Job::where($columnName,$filtervalue)->first();
		return $resJob;
	}



	/*
	 * {@inheritdoc}
	 */
	public function getAll()
	{
		 

		$jobDataCollection = Job::with('taggedUsers')->with('jobdetails.salaryBand')->with('jobdetails.sector')->with('jobdetails.reward')->with('jobdetails.country')->with('jobdetails.client')->with('jobdetails.activePeriod')->with('jobdetails.publishStatus')->get();
		dd($jobDataCollection);

		$jobDetail = array();
		$i=0;
		foreach($jobDataCollection as $jobData){
			/*Job data*/
			
			$jobDetail[$i]['url'] = $jobData->url;
			$jobDetail[$i]['title'] = $jobData->title;
			$jobDetail[$i]['description'] = $jobData->description;
			$jobDetail[$i]['location'] = $jobData->location;
			$jobDetail[$i]['apply_url'] = $jobData->apply_url;
			$jobDetail[$i]['video_url'] = $jobData->video_url;
			$jobDetail[$i]['reference_number'] = $jobData->reference_number;
			$jobDetail[$i]['vacancy_reason'] = $jobData->vacancy_reason;
			$jobDetail[$i]['isDocument_mandatory'] = $jobData->isDocument_mandatory;
			$jobDetail[$i]['salary_details'] = $jobData->salary_details;
			$jobDetail[$i]['video_image_link'] = $jobData->video_image_link;
			$jobDetail[$i]['quick_description'] = $jobData->quick_description;
			$jobDetail[$i]['hr_description'] = $jobData->hr_description;
			$jobDetail[$i]['agency_fee'] = $jobData->agency_fee;
			$jobDetail[$i]['document_path'] = $jobData->document_path;
			$jobDetail[$i]['referal_reward_description'] = $jobData->referal_reward_description;
			$createdAt = Carbon::parse($jobData->created_at);
			$jobDetail[$i]['created_at'] = $createdAt->format('d M, Y h:i a');
			$jobDetail[$i]['internal_notes'] = $jobData->internal_notes;
			$jobDetail[$i]['vacancy_count'] = $jobData->vacancy_count;

			/*job detail data*/
			$jobDetail[$i]['auto_repost'] = $jobData->jobdetails->auto_repost;
			$jobDetail[$i]['campaign_id'] = $jobData->jobdetails->campaign_id;
			$jobDetail[$i]['salary_band_id'] = $jobData->jobdetails->salary_band_id;
			$jobDetail[$i]['salary'] = $jobData->jobdetails->salaryBand->currency." ".$jobData->jobdetails->salaryBand->start_value."-".$jobData->jobdetails->salaryBand->end_value;
			$jobDetail[$i]['sector_id'] = $jobData->jobdetails->sector_id;
			$jobDetail[$i]['sector'] = $jobData->jobdetails->sector->description;
			$jobDetail[$i]['published_status_id'] = $jobData->jobdetails->published_status;
			$jobDetail[$i]['published_status'] = $jobData->jobdetails->publishStatus->description;
			$jobDetail[$i]['reward_id'] = $jobData->jobdetails->reward_id;
			$jobDetail[$i]['reward'] = $jobData->jobdetails->reward->title." ".$jobData->jobdetails->reward->type;
			$jobDetail[$i]['country_id'] = $jobData->jobdetails->country_id;
			$jobDetail[$i]['country'] = $jobData->jobdetails->country->country_name;			
			$jobDetail[$i]['count_shortlisted'] = $jobData->jobdetails->count_shortlisted;
			$jobDetail[$i]['source_detail'] = $jobData->jobdetails->source_detail;
			$jobDetail[$i]['count_new'] = $jobData->jobdetails->count_new;
			$jobDetail[$i]['count_review'] = $jobData->jobdetails->country_id;
			/*$expireAt = Carbon::parse($jobData->jobdetails->expiry_at);
			$jobDetail[$i]['expiry_at'] = $expireAt->format('d M, Y h:i a');*/
			$jobDetail[$i]['client_id'] = $jobData->jobdetails->clients_id;
			$jobDetail[$i]['active_period_id'] = $jobData->jobdetails->active_period_id;
			$jobDetail[$i]['activeDays'] = date("d",$jobData->jobdetails->activePeriod->time_to_expiry);

			/*Pivot table data*/

			$t=0;
			if(count($jobData->tags)>0){
				foreach($jobData->tags as $tagsData){
					$jobDetail[$i]['tags'][$t]['id'] = $tagsData->id;
					$jobDetail[$i]['tags'][$t]['title'] = $tagsData->title;
					$t++;
				}
		    }else{
		    	$jobDetail[$i]['tags'] = "";
		    }
		    $c=0;
			if(count($jobData->contractType)>0){
				foreach($jobData->contractType as $contractType){
					$jobDetail[$i]['contract'][$c]['id'] = $contractType->id;
					$jobDetail[$i]['contract'][$c]['type'] = $contractType->contract_type;
					$c++;
				}
		    }else{
		    	$jobDetail[$i]['contract'] = "";
		    }	
		    $u=0;

			if(count($jobData->taggedUsers)>0){
				foreach($jobData->taggedUsers as $taggedUsers){
					$jobDetail[$i]['user'][$u]['id'] = $taggedUsers->id;
					$jobDetail[$i]['user'][$u]['name'] = $taggedUsers->prefer_name;
					$jobDetail[$i]['user'][$u]['type'] = $taggedUsers->pivot->role_name;
					$u++;
				}
		    }else{
		    	$jobDetail[$i]['user'] = "";
		    }	


			$i++;
		}
		return $jobDetail;
	}


	/**
	* insert data in job, job_detail and other pivot tables
	* @author Yamin
	* @param array $jobData
	* @return $jobId
	**/

	public function create($jobData){
		$url = str_slug($jobData->json()->get('title'), "-");
		$activeDate = "";
		$activePeriodId = 0;
		/*if expiry at is not blank then only job will be live*/
		if($jobData->json()->get('expiry_at') != ""){
			$activeDate = time();
			$activeTime = strtotime($jobData->json()->get('expiry_at')) - time();
			$ActivePeriod = ActivePeriod::Create(['time_to_expiry' => $activeTime]);
			$activePeriodId = $ActivePeriod->id; 
		}
		/*insert in to job table*/
		$job = new Job();
		$job->url = $url;
		$job->title = $jobData->json()->get('title');
		$job->description = $jobData->json()->get('description');
		$job->location = $jobData->json()->get('location');
		$job->apply_url = $jobData->json()->get('apply_url');
		$job->video_url = $jobData->json()->get('video_url');
		$job->reference_number = $jobData->json()->get('reference_number');
		$job->vacancy_reason = $jobData->json()->get('vacancy_reason');
		$job->isDocument_mandatory = $jobData->json()->get('isDocument_mandatory');
		$job->video_image_link = $jobData->json()->get('video_image_link');
		$job->salary_details = $jobData->json()->get('salary_details');
		$job->quick_description = $jobData->json()->get('quick_description');
		$job->hr_description = $jobData->json()->get('hr_description');
		$job->agency_fee = $jobData->json()->get('agency_fee');
		/*TODO: wirte the logic for upload document*/
		$job->document_path = $jobData->json()->get('document');
		$job->referal_reward_description = $jobData->json()->get('referal_reward_description');
		$job->internal_notes = $jobData->json()->get('internal_notes');
		$job->vacancy_count = $jobData->json()->get('vacancy_count');	
		$job->state_update_date = $activeDate;
		$job->save();
		$jobId = $job->id;

		/*TODO:if refernce number not exist then update the ID of the job in refernce number*/
		

		/*insert in to job detail table*/
		$jobDetail = new JobDetails();
		$jobDetail->job_id = $jobId;
		$jobDetail->auto_repost = $jobData->json()->get('auto_repost');
		$jobDetail->campaign_id = $jobData->json()->get('campaign_id');
		$jobDetail->salary_band_id = $jobData->json()->get('salary_band_id');
		$jobDetail->sector_id = $jobData->json()->get('sector_id');
		$jobDetail->published_status = $jobData->json()->get('published_status');
		$jobDetail->reward_id = $jobData->json()->get('reward_id');
		$jobDetail->country_id = $jobData->json()->get('country_id');
		$jobDetail->source_detail = $jobData->json()->get('source_detail');
		$jobDetail->expiry_at = strtotime($jobData->json()->get('expiry_at'));
		$jobDetail->clients_id = $jobData->json()->get('clients_id');
		$jobDetail->active_period_id = $activePeriodId;
		$jobDetail->created_by = $jobData->json()->get('created_by');
		$jobDetail->save();

		/*Insert into Pivot table*/
		$job->tags()->attach($jobData->json()->get('job_tags'));
		$job->contractType()->attach($jobData->json()->get('contract_type'));	

		$job->taggedUsers()->attach($jobData->json()->get('hr_manager_ids'),['role_name' => 'hr']);
		$job->taggedUsers()->attach($jobData->json()->get('recruiter_ids'),['role_name' => 'recruiter']);
		$job->taggedUsers()->attach($jobData->json()->get('agency_user_ids'),['role_name' => 'agency']);

		return $jobId;
	}

}
