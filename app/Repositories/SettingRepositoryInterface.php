<?php

namespace App\Repositories;

/**
 * Interface SettingRepositoryInterface
 * @package App\Repositories
 */
interface SettingRepositoryInterface {

	
	public function addSetting($settingData);
	
	public function editSetting($settingData);

	public function listSetting($settingData);


}
