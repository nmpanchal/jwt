<?php

namespace App\Repositories;
use App\Model\Clients\Setting;

use Carbon\Carbon;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;


/**
 * Interface SettingRepositoryInterface
 * @package App\Repositories
 */
Class SettingRepository implements SettingRepositoryInterface {

	protected $id;

	/**
	*Add Setting
	*@author Yamin
	*@param $settingData
	*@return $settingId
	**/
	public function addSetting($settingData){
		$setting = new Setting();
		$setting->type = $settingData['type'];
		$setting->value = $settingData['value'];
		$setting->save();
		return $setting->id;
	}

	/**
	*Edit Setting
	*@author Yamin
	*@param $settingData
	*@return boolean
	**/
	public function editSetting($settingData){
		$setting = Setting::find($settingData['id']);
		if(!$setting){
			return false;
		}
		$setting->fill(['type'=>$settingData['type'],'value'=>$settingData['value']]);
		return $setting->save();
	}

	/**
	*List Setting
	*@author Yamin
	*@param $settingData
	*@return Array
	**/
	public function listSetting($settingData){
		if($settingData['type'] == "all"){
			$setting = Setting::all();
			return $setting;
		}
		$setting = Setting::where('type',$settingData)->get();
		return $setting;
	}

	/**
	*Delete Setting
	*@author Yamin
	*@param $settingData
	*@return boolean
	**/
	public function removeSetting($settingData){
		$setting = Setting::find($settingData['id']);
		if(!$setting){
			return false;
		}
		return $setting->delete();
	}

	

}
