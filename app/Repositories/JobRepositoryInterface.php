<?php

namespace App\Repositories;

/**
 * Interface JobRepositoryInterface
 * @package App\Repositories
 */
interface JobRepositoryInterface {

	/**
	 * Get job by id
	 * @param $jobid
	 * @return mixed
	 */
	public function getById($jobid);

  /**
  * filter by client / application / candidate
  * @param $filtername
  * @param $limit
  * @param $offset
  **/
	public function getByFilter($filtername, $filtervalue, $limit = NULL, $offset = NULL);
}
