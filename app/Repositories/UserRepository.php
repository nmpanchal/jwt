<?php

namespace App\Repositories;
use App\Model\User;
use App\Model\Client;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Helper;

/**
 * Interface UserRepositoryInterface
 * @package App\Repositories
 */
Class UserRepository implements UserRepositoryInterface {

	protected $id;
	/**
	 * Signup user from front, Will make entry in client, user
	 *@author Yamin
	 *@param $userData (JSON)
	 *@return $userId
	 */
	public function addUserFront($userData){
		$emailExp = explode('@',$userData->json()->get('email'));
		$domain = Helper::verifyDomain($emailExp[1]);
		if($domain == "INVALID"){
			/*TODO: check false is ok need to return some text for specific message*/
			return false;
		}
		try{
			$client = new Client();
			$client->client_name = $userData->json()->get('company');
			$client->comments = $userData->json()->get('comments');
			$client->save();
			$clientId = $client->id;
	    }catch(Exception $e){
              /*TODO:Write in log*/
        }

        try{
			$user = new User();
			$user->prefer_name = $userData->json()->get('first_name')." ".$userData->json()->get('last_name');
			$user->login_email = $userData->json()->get('email');
			$user->is_verified = 0;
			$user->activation_token = Hash::make(uniqid());
			$user->phone = $userData->json()->get('phone_number');
			$user->job_title = $userData->json()->get('job_title');
			$user->client_id = $clientId;
			$user->save();
		 }catch(Exception $e){
              /*TODO:Write in log*/
         }
         return $user->id;
	}
	/**
	 * Verify activate token (Dont't return everything here only return required info)
	 *@author Yamin
	 *@param $token
	 *@return Array (UserDetail)
	 */
	public function verifyActivateToken($token){
		 $userDetail = User::with('client')->where('activation_token',$token)->first();
		 $userData = array();
		 if($userDetail){
		 	/*User detail*/
            $userData['prefer_name'] = $userDetail->prefer_name;
            $userData['phone'] = $userDetail->phone;
            $userData['job_title'] = $userDetail->job_title;

            /*client detail*/
            $userData['client_id'] = $userDetail->client_id;
            $userData['client_name'] = $userDetail->client_name;
         }
         return $userData;
	}

	/**
	 * Verify activate token
	 *@author Yamin
	 *@param $token
	 *@return Array (UserDetail)
	 */
	public function updateActivatedUser($userData){
		 /*update data in User*/
		 try{
			 $user = User::find($userData->json()->get('user_id'));
			 $user->prefer_name = $userData->json()->get('prefer_name');
			 $user->login_password = sha1($userData->json()->get('login_password'));
			 $user->phone = $userData->json()->get('phone');
			 $user->job_title = $userData->json()->get('job_title');
			 $user->is_verified = 1;
			 $user->api_secret = uniqid();
			 $user->last_online = date("Y-m-d H:i:s");
			 $userSave = $user->save();
			 $userId = $user->id;
		 }catch(Exception $e){
                /*TODO:Write in log*/
         }
		 /*update data in client*/
		 try{
			 $client = Client::find($user->client_id);
			 $client->client_name = $userData->json()->get('client_name');
			 $client->logo_url = "";
			 if($userData->hasFile('logo')){
				 /*TODO: I assume to get file in simple post not in JSON if in JSON then do change here */
				 $fileArray = Helper::uploadFile($userData->file('logo'),'clientLogo/',$user->client_id);
				 if(count($fileArray)>0){
				 	$client->logo_url = $fileArray['fullPath'];
				 }
			 }
			 $client->client_url = $userData->json()->get('client_url');
			 $client->seo_url = $userData->json()->get('seo_url');
			 $client->country_id = $userData->json()->get('country_id');
			 $client->comments = $userData->json()->get('comments');
			 $clientSave = $client->save();
		 }catch(Exception $e){
                /*TODO:Write in log*/
         }
         if($userSave && $clientSave){
         	return $userId;
         }
         return false;
	}


	public function getById($userid)
	{
		return User::where('activation_link');
	}


	/**
	 * @param $username
	 * @param $password
	 */
	public function getByCredentials($useremail, $password)
	{
		$resUser = User::where('login_email',$useremail)->first();
		return $resUser;
	}


	/**
	 * @param $token
	 *
	 */
	public function getRoleByToken($token)
	{
		$resUser = User::where('api_secret',$token)->first();
		return $resUser;
	}




	/*
	 * {@inheritdoc}
	 */
	public function getAll()
	{
		return User::all();
	}


	/**
	 * set preferred name
	 * @param $userid
	 * @param $name
	 */
	public function setPreferName($userid, $name){
		$user = User::find($userid);
		$user->prefer_name = $name;
		$user->save();
		return $user;
	}

	/**
	 * set preferred name
	 * @param $userid
	 *
	 */
	public function setLastLogin($userid){
		$user = User::find($userid);
		$user->last_login = date();
		$user->save();
		return $user;
	}

	/**
	* Create a new user
	* @param array $data
	* TODO: anyone with API token can create users (it should be admin)
	***/

	public function create(array $data){

		return User::create([
				'prefer_name' => $data['name'],
				'login_email' => $data['email'],
				'login_password' => bcrypt($data['password']),
				'api_secret' =>str_random(10),
				'last_online' => Carbon::now(),
				'phone' => (isset($data['phone'])? $data['phone'] : NULL)
		]);
	}

	/**
	* Lookup for the domain name if exists return the token to be used
	* @param string $domain
	* @return mixed
	**/
	public function getByDomain($domain){
		$resUser = User::where('domain_name',$domain)->first();
		if($resUser){
				$data = array('table'=>$resUser->database_name, 'api_secret' =>$resUser->api_secret);
		} else {
			 $data = array('error' => 'Your email has not been signed up yet for services, contact hi@talentadvocate.com');
		}
		return $data;
	}
}
