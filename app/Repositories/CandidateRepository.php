<?php

namespace App\Repositories;
use App\Model\Candidate;
use App\Model\ContractType;
use App\Model\SalaryBand;
use App\Model\FkCandidateSalary;
use App\Model\FkCandidateContract;
use Carbon\Carbon;
use Illuminate\Http\Response;
use Helper;
use Illuminate\Support\Facades\Log;


/**
 * Interface CandidateRepositoryInterface
 * @package App\Repositories
 */
Class CandidateRepository implements CandidateRepositoryInterface {

	protected $id;


	/**
	 *  Single candidate detail		
	 *@author Yamin
	 *@param  $candidateId
	 *@return Array contain single candidate detail 
	 **/
	public function getById($candidateId)
	{

		$candidateDetail = Candidate::with('country')->with('sector')->with('educationLevel')->with('careerLevel')->with('candidateContract')->with('candidateSalary')->find($candidateId);

		$candidateData = array();
		if(!$candidateDetail){ return $candidateData;}

				$candidateData['first_name'] = $candidateDetail->first_name;
				$candidateData['last_name'] = $candidateDetail->last_name;
				$candidateData['address1'] = $candidateDetail->address1;
				$candidateData['address2'] = $candidateDetail->address2;
				$candidateData['town'] = $candidateDetail->town;
				$candidateData['postcode'] = $candidateDetail->postcode;
				$candidateData['phone'] = $candidateDetail->phone;
				$candidateData['email'] = $candidateDetail->email;
				$candidateData['uk_passport'] = $candidateDetail->uk_passport;
				$candidateData['profile_pic'] = $candidateDetail->profile_pic;

				$candidateData['career_level_id'] = $candidateDetail->career_level_id;
				$candidateData['career_level_name'] = $candidateDetail->careerLevel->title;

				$candidateData['source_details'] = Helper::jsonDeserialize($candidateDetail->source_details);
				$candidateData['registration_details'] = Helper::jsonDeserialize($candidateDetail->registration_details);

				$candidateData['status'] = $candidateDetail->status;
				/*get the multiple contract type from relational table*/
				if(count($candidateDetail->candidateContract)>0){
					$i=0;
					foreach($candidateDetail->candidateContract as $contractType){
						$contractTypeDetail = ContractType::find($contractType->contracttype_id);
						if(count($contractTypeDetail)>0){
							$candidateData['contract_type'][$i] = $contractTypeDetail->contract_type; 
							$i++;
						}
					}
				}else{
					$candidateData['contract_type'] = "";	
				}
				$candidateData['licence_available'] = $candidateDetail->licence_available;
				/*get the multiple Salary from relational table*/
				if(count($candidateDetail->candidateSalary)>0){
					$i=0;
					foreach($candidateDetail->candidateSalary as $salaryBand){
						$salaryBandDetail = SalaryBand::find($salaryBand->salary_band_id);
						if(count($salaryBandDetail)>0){
							$candidateData['salary_sought'][$i]['value'] = $salaryBandDetail->start_value." - ".$salaryBandDetail->end_value; 
							$candidateData['salary_sought'][$i]['currency'] = $salaryBandDetail->currency;
							$candidateData['salary_sought'][$i]['comments'] = $salaryBandDetail->comments; 
							$i++;
						}
					}
				}else{
					$candidateData['salary_sought'] = "";	
				}
				$candidateData['notice_period'] = $candidateDetail->notice_period;
				$candidateData['availability'] = $candidateDetail->availability;
				$candidateData['ready_to_relocate'] = $candidateDetail->ready_to_relocate;
				$candidateData['bio_details'] = Helper::jsonDeserialize($candidateDetail->bio_details);
				$candidateData['exp_details'] = Helper::jsonDeserialize($candidateDetail->exp_details);
				$candidateData['education_level_id'] = $candidateDetail->education_level_id;
				$candidateData['education_level_name'] = $candidateDetail->educationLevel->title;
				$candidateData['education_details'] = Helper::jsonDeserialize($candidateDetail->education_details);
				$candidateData['reset_token'] = $candidateDetail->reset_token;
				$candidateData['cv_details'] = Helper::jsonDeserialize($candidateDetail->cv_details);
				$candidateData['created_at'] = Carbon::parse($candidateDetail->created_at)->format('d-m-Y h:m a');
				$candidateData['updated_at'] = Carbon::parse($candidateDetail->updated_at)->format('d-m-Y h:m a');
				$candidateData['country_id'] = $candidateDetail->country_id;
				$candidateData['country_name'] = $candidateDetail->country->country_name;
				$candidateData['sector_id'] = $candidateDetail->sector_id;
				$candidateData['sector_name'] = $candidateDetail->sector->description;
				$candidateData['honor_title'] = $candidateDetail->honor_title;
				return $candidateData;
	}


	/**
	 * @param $filtername
	 *
	 */
	public function getByFilter($filtername , $filtervalue, $limit = NULL, $offset = NULL)
	{
    // This will be used for filtering job based on conditions
    	$columnName = $filtername.'id';
		$resCandidate = Candidate::where($columnName,$filtervalue)->first();
		return $resCandidate;
	}



	/**
	 *TODO: Give list of all candidate
	 *@author Yamin
	 *@param  NULL
	 *@return Array contain candidate detail 
	 {
		"per_page_record":10	
	 }
	 **/
	public function getAll($data)
	{

		$candidate = Candidate::with('country')->with('sector')->with('educationLevel')->with('careerLevel')->with('candidateContract')->with('candidateSalary')->paginate($data->json()->get('per_page_record'));
		$candidateData = array();
		$i=0;
		if(!$candidate){ return $candidate;}
			/*paging Records*/
			$candidateData['paginator']['totalRecord'] = $candidate->total();
			$candidateData['paginator']['perPage'] = $candidate->perPage();
			$candidateData['paginator']['currentPage'] = $candidate->currentPage();
			$candidateData['paginator']['lastPage'] = $candidate->lastPage();

			foreach($candidate as $candidateDetail){

				$candidateData[$i]['first_name'] = $candidateDetail->first_name;
				$candidateData[$i]['last_name'] = $candidateDetail->last_name;
				$candidateData[$i]['address1'] = $candidateDetail->address1;
				$candidateData[$i]['address2'] = $candidateDetail->address2;
				$candidateData[$i]['town'] = $candidateDetail->town;
				$candidateData[$i]['postcode'] = $candidateDetail->postcode;
				$candidateData[$i]['phone'] = $candidateDetail->phone;
				$candidateData[$i]['email'] = $candidateDetail->email;
				$candidateData[$i]['uk_passport'] = $candidateDetail->uk_passport;
				$candidateData[$i]['profile_pic'] = $candidateDetail->profile_pic;
				$candidateData[$i]['career_level_id'] = $candidateDetail->career_level_id;
				$candidateData[$i]['career_level_name'] = $candidateDetail->careerLevel->title;
				$candidateData[$i]['source_details'] = Helper::jsonDeserialize($candidateDetail->source_details);
				$candidateData[$i]['registration_details'] = Helper::jsonDeserialize($candidateDetail->registration_details);
				$candidateData[$i]['status'] = $candidateDetail->status;
				/*get the multiple contract type from relational table*/
				if(count($candidateDetail->candidateContract)>0){
					$j=0;
					foreach($candidateDetail->candidateContract as $contractType){
						$contractTypeDetail = ContractType::find($contractType->contracttype_id);
						if(count($contractTypeDetail)>0){
							$candidateData[$i]['contract_type'][$j] = $contractTypeDetail->contract_type; 
							$j++;
						}
					}
				}else{
					$candidateData[$i]['contract_type'] = "";	
				}
				$candidateData[$i]['licence_available'] = $candidateDetail->licence_available;
				/*get the multiple Salary from relational table*/
				if(count($candidateDetail->candidateSalary)>0){

					$j=0;
					foreach($candidateDetail->candidateSalary as $salaryBand){
						$salaryBandDetail = SalaryBand::find($salaryBand->salary_band_id);
						if(count($salaryBandDetail)>0){
							$candidateData[$i]['salary_sought'][$j]['value'] = $salaryBandDetail->start_value." - ".$salaryBandDetail->end_value; 
							$candidateData[$i]['salary_sought'][$j]['currency'] = $salaryBandDetail->currency;
							$candidateData[$i]['salary_sought'][$j]['comments'] = $salaryBandDetail->comments; 
						    $j++;
						}
					}
				}else{
					$candidateData[$i]['salary_sought'] = "";	
				}
				$candidateData[$i]['notice_period'] = $candidateDetail->notice_period;
				$candidateData[$i]['availability'] = $candidateDetail->availability;
				$candidateData[$i]['ready_to_relocate'] = $candidateDetail->ready_to_relocate;
				$candidateData[$i]['bio_details'] = Helper::jsonDeserialize($candidateDetail->bio_details);
				$candidateData[$i]['exp_details'] = Helper::jsonDeserialize($candidateDetail->exp_details);
				$candidateData[$i]['education_level_id'] = $candidateDetail->education_level_id;
				$candidateData[$i]['education_level_name'] = $candidateDetail->educationLevel->title;
				$candidateData[$i]['education_details'] = Helper::jsonDeserialize($candidateDetail->education_details);
				$candidateData[$i]['reset_token'] = $candidateDetail->reset_token;
				$candidateData[$i]['cv_details'] = Helper::jsonDeserialize($candidateDetail->cv_details);
				$candidateData[$i]['created_at'] = Carbon::parse($candidateDetail->created_at)->format('d-m-Y h:m a');
				$candidateData[$i]['updated_at'] = Carbon::parse($candidateDetail->updated_at)->format('d-m-Y h:m a');
				$candidateData[$i]['country_id'] = $candidateDetail->country_id;
				$candidateData[$i]['country_name'] = $candidateDetail->country->country_name;
				$candidateData[$i]['sector_id'] = $candidateDetail->sector_id;
				$candidateData[$i]['sector_name'] = $candidateDetail->sector->description;
				$candidateData[$i]['honor_title'] = $candidateDetail->honor_title;
				
				$i++;
			}

			
		return $candidateData;

	}


	/**
	 * There can be some imporvements because of length of it
	 * TODO: Add Candidate
	 * @author Yamin
	 * @param $candidateData
	 * @return candidateId
	 **/
	public function create($candidateData){
				$fileName = $candidateId;
				$fileArray = Helper::uploadFile($request->file('cvFile'),'cv/',$fileName);


			try{

					$emailExp = explode('@',$candidateData->json()->get('email'));
					$domain = Helper::verifyDomain($emailExp[1]);
					if($domain == "INVALID"){
						/*TODO: check false is ok need to return some text for specific message*/
						return false;
					}
					$candidate = new Candidate();

					// General
		            $candidate->first_name    = $candidateData->json()->get('first_name');
		            $candidate->last_name    = $candidateData->json()->get('last_name');
		            $candidate->email        = $candidateData->json()->get('email');
		            $candidate->password     = sha1($candidateData->json()->get('password'));
		            $candidate->profile_pic  = $candidateData->json()->get('profile_pic');
		            $candidate->honor_title  = $candidateData->json()->get('honor_title');
		            
		            // Address
		            $candidate->address1    = $candidateData->json()->get('address1');
		            $candidate->address2    = $candidateData->json()->get('address2');
		            $candidate->town        = $candidateData->json()->get('town');
		            $candidate->postcode    = $candidateData->json()->get('postcode');
		            $candidate->country_id  = $candidateData->json()->get('country_id');
		            $candidate->phone    	= $candidateData->json()->get('phone');

		            // Experience

		            $candidate->exp_details = Helper::jsonSerialize($candidateData->json()->get('experience'));
		            // Education
		            $candidate->education_details  = Helper::jsonSerialize($candidateData->json()->get('education'));

		            //Registration 
		            $registration = array();
		            $registration['registration']    = $candidateData->json()->get('registration');
		            $registration['reg_token']       = $candidateData->json()->get('reg_token');
		            $registration['ip_address']      = $candidateData->json()->get('ip_address');
		            $registration['browser']         = $candidateData->json()->get('browser');
		            $candidate->registration_details = Helper::jsonSerialize($registration);

		            //Availibility 
		            $candidate->notice_period      = $candidateData->json()->get('notice_period');
		            $candidate->availability       = $candidateData->json()->get('availability');
		            $candidate->licence_available  = $candidateData->json()->get('licence_available');           
		            $candidate->status             = $candidateData->json()->get('status');
		            $candidate->ready_to_relocate  = $candidateData->json()->get('ready_to_relocate');
		            //bio
		            $bio = array();
		            $bio['hobby'] = $candidateData->json()->get('hobby');
		            $bio['bio']   = $candidateData->json()->get('bio');
		            $candidate->bio_details = Helper::jsonSerialize($bio);

		            //Source Detail
		            $source = array();
		            $source['source_id'] = $candidateData->json()->get('source_site_id');
		            $source['source_detail'] = $candidateData->json()->get('source_detail');
		            $candidate->source_details = Helper::jsonSerialize($source);

		            $candidate->uk_passport        = $candidateData->json()->get('uk_passport');
		            $candidate->career_level_id    = $candidateData->json()->get('career_level_id');
				    $candidate->education_level_id = $candidateData->json()->get('education_level_id');
		            $candidate->reset_token        = $candidateData->json()->get('reset_token');
		            $candidate->sector_id          = $candidateData->json()->get('sector_id');

		            //save details in candidate table
		            $candidate->save();

	            }catch(Exception $e){
              		Log::error($e->getMessage());
        		}

	            /*Add multiple values in relational table with candidateID*/
	            try{
		            $fkCandidateSalary = new FkCandidateSalary();
		            foreach($candidateData->salary_sought as $salarySought){
						$fkCandidateSalary->addCandidateSalary($candidate->id,$salarySought);	
					}

					$fkCandidateContract = new FkCandidateContract();
		            foreach($candidateData->contract_type as $contractType){
						$fkCandidateContract->addCandidateContractType($candidate->id,$contractType);	
					}
				}catch(Exception $e){
              		/*TODO:Write in log*/
        		}	

            return $candidate->id;

		
	}
	/**
	 * Its just a looped function which save our lines of code and also give the dynamism in terms of edit as much field as you want
	 * TODO: Edit Candidate
	 * @author Yamin
	 * @param $candidateData
	 * @return boolean
	 **/
	public function edit($candidateData){
		$candidateId = $candidateData->json()->get('id');
		$candidate = Candidate::find($candidateId);
		$candidateDatas = $candidateData->json()->all();
		
		$fillArray = array();
		$registration = array();
		$bio = array();
		try{
			foreach($candidateDatas as $candidateDetailKey => $candidateDetailValue){
				if($candidateDetailKey == "education" || $candidateDetailKey == "experience"){
					$key = ($candidateDetailKey == "education") ? "education_details":"exp_details";
					$fillArray[$key] = Helper::jsonSerialize($candidateData->json()->get($candidateDetailKey));

				}else if($candidateDetailKey == "hobby" || $candidateDetailKey == "bio"){

					$bio[$candidateDetailKey] = $candidateData->json()->get($candidateDetailKey);

				}else if($candidateDetailKey == "salary_sought"){
					if(count($candidateData->salary_sought)>0){
						$fkCandidateSalary = new FkCandidateSalary();
						$fkCandidateSalary->removeCandiadteSalary($candidateId);
			            foreach($candidateData->salary_sought as $salarySought){
							$fkCandidateSalary->addCandidateSalary($candidateId,$salarySought);	
						}	
					}
				}else if($candidateDetailKey == "contract_type"){
					if(count($candidateData->contract_type)>0){
						$fkCandidateContract = new FkCandidateContract();
						$fkCandidateContract->removeCandiadteContract($candidateId);
			            foreach($candidateData->contract_type as $contractType){
							$fkCandidateContract->addCandidateContractType($candidate->id,$contractType);	
						}	
					}
				}else{
					$fillArray[$candidateDetailKey] = $candidateData->json()->get($candidateDetailKey);
				}
			}
			$fillArray['bio_details'] = Helper::jsonSerialize($bio);

			$candidate->fill($fillArray);
			return $candidate->save();
		}catch(Exception $e){
              /*TODO:Write in log*/
        }
		

	}
	/**
	*update password, confirm old password from email and ID first and then change new password	
	*@author Yamin
	*@param $password detail (JSON ARRAY)
	*@return boolean
	**/
	public function updatePassword($passwordDetail){
		$candidateDetail = Candidate::where('id', '=', $passwordDetail->json()->get('candidate_id'))->where('email', '=', $passwordDetail->json()->get('email'))->where('password', '=', sha1($passwordDetail->json()->get('old_password')))->first();
		if(!$candidateDetail){
			return "wrong detail";
		}
		try{
			$candidate = Candidate::find($passwordDetail->json()->get('candidate_id'));
			$candidate->password = sha1($passwordDetail->json()->get('new_password'));
			$passwordSave = $candidate->save();
		}catch(Exception $e){
			 /*TODO:Write in log*/
		}
		if($passwordSave){
			return "updated";
		}

	}

	public function uploadCV($cvData){
		    if(!$cvData->hasFile('cv_file')){
		        return false;
		    }
		    /*TODO: I assume to get file in simple post not in JSON if in JSON then do change here */
			$fileArray = Helper::uploadFile($cvData->file('cvFile'),'cv/',$cvData->json()->get('candidate_id'));
			if(count($fileArray) == 0){
			   return false;	
			}
			try{
				$candidate = Candidate::find($cvData->json()->get('candidate_id'));
			    $cv = array();
			    $cv['cv_name']  = $fileArray['fileName'];
			    $cv['type']     = $fileArray['mimeType'];
			  	$cv['path']     = $fileArray['fullPath'];
			    $cv['uploaded'] = "No";
			    $candidate->fill(array('cv_details' => Helper::jsonSerialize($cv)));
			}catch(Exception $e){
				/*TODO:Write log here*/	
			}    
			return $candidate->save();
		    

	}

}
