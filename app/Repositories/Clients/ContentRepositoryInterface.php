<?php
namespace App\Repositories\Clients;

/**
 * Interface ContentRepositoryInterface
 * @package App\Repositories\Clients
 */
interface ContentRepositoryInterface {

	/**
	 * Get All Content for Admin
	 * @param none
	 * @return mixed
	 */
	public function getAllForAdmin();

	/**
	 * Get All Content for Admin
	 * @param array $user   (might be all department head so all users under him)
 	 * @return mixed
	 */
	public function getAll(array $user, $department, $approved=null);


  /**
  * filter by Other Params for Content
  * @param $filtername
  * @param $limit
  * @param $offset
  **/
	public function getByFilter($filtername, $filtervalue, $limit = NULL, $offset = NULL);
}
