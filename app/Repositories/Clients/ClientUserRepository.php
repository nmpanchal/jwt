<?php

namespace App\Repositories\Clients;

use App\Model\Clients\TenantClientUsers;
use App\Model\Clients\Departments;
use App\Repositories\Clients\ClientUserRepositoryInterface;


/**
 * Interface ClientUserRepositoryInterface
 * @package App\Repositories\Clients
 */

 Class ClientUserRepository implements ClientUserRepositoryInterface {

   protected $id;

   public function getByToken($token){

     $user = TenantClientUsers::where('auth_token','=',$token)->first();
     return $user;
   }


   public function getByFilter($filtername, $filtervalue, $limit = NULL, $offset = NULL){
     // TODO : Filters for User incase needed
     return true;
   }




 }
