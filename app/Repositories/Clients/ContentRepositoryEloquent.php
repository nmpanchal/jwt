<?php
namespace App\Repositories\Clients;

use App\Model\Clients\Content;
use App\Model\Clients\ApprovalType;
use DB;


Class ContentRepositoryEloquent implements ContentRepositoryInterface {


	public function getAllForAdmin(){

    return true;
  }

	public function getAll(array $user, $department, $approved = null){
    $approvedOrNot = (count($approved))?$approved:0;

    $data = Content::where('department_id','=',$department)
                  ->where('approval','=',$approvedOrNot)
                  ->whereIn('user_id',$user);
    return $data;
  }


  /**
  * filter by Other Params for Content
  * @param $filtername
  * @param $limit
  * @param $offset
  **/
	public function getByFilter($filtername, $filtervalue, $limit = NULL, $offset = NULL){
    return true;
  }

	/**
  * create content
  * @param $user, $department, $requestdata
  * @return mixed
  **/
	public function create($userId, $department, $reqData){
					DB::beginTransaction();
			try {
					/*$content = new Content;
					//dd($content);
					$content->user_id = $userId;
					$content->department_id = $department;
					$content->title = $reqData['title'];
					$content->description = $reqData['description'];
					$content->image_url = $reqData['media'];
					$content->approval = 0;
					$content->approval_flag= 1; //pending
					//$content->fill($reqData); //named mapping & fill() can be used
					$content->save(); */

					$content = Content::firstOrCreate(
								['user_id' => $userId,
								'department_id' => $department,
								'title' => $reqData['title'],
								'description' => $reqData['description'],
								'image_url' => $reqData['media'],
								'approval' => 0,
								'approval_flag'=> 1
							]);

			} catch(\Exception $e) {
					DB::rollBack();
					$content = array("error"=>"Something went wrong with insert, probably query?".$e);
			}
			DB::commit();
			return $content;
	}


	/**
	* update/approve content
	* @param $approval, $approval_flag, $id
	* @return mixed
	**/
	public function update($contentId, $reqData){
			DB::beginTransaction();
			try {
					$content = Content::whereId($contentId)->first();
          $content->approval = $reqData['approved'];
					$content->approval_flag = $reqData['approval_type'];
					$content->save();

			} catch(\Exception $e) {
					DB::rollBack();
					$content = array("error"=>"Something went wrong with update, probably query?".$e);
			}
			DB::commit();
			return $content;
	}


}
