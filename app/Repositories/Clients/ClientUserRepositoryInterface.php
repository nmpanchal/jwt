<?php
namespace App\Repositories\Clients;

/**
 * Interface ClientUserRepositoryInterface
 * @package App\Repositories\Clients
 */
interface ClientUserRepositoryInterface {

	/**
	 * Get User details by Token
	 * @param $token
	 * @return mixed
	 */
	public function getByToken($token);

  /**
  * filter by Other Params for User
  * @param $filtername
  * @param $limit
  * @param $offset
  **/
	public function getByFilter($filtername, $filtervalue, $limit = NULL, $offset = NULL);
}
