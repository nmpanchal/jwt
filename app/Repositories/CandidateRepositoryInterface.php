<?php

namespace App\Repositories;

/**
 * Interface CandidateRepositoryInterface
 * @package App\Repositories
 */
interface CandidateRepositoryInterface {

	/**
	 * Get job by id
	 * @param $candidateid
	 * @return mixed
	 */
	public function getById($candidateId);

  /**
  * filter by client / application 
  * @param $filtername
  * @param $limit
  * @param $offset
  **/
	public function getByFilter($filtername, $filtervalue, $limit = NULL, $offset = NULL);
}
