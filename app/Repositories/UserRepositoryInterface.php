<?php

namespace App\Repositories;

/**
 * Interface ApplicantRepositoryInterface
 * @package App\Repositories
 */
interface UserRepositoryInterface {

	/**
	 * Get user by id
	 * @param $userid
	 * @return mixed
	 */
	public function getById($userid);

	public function setPreferName($userid, $name);

	public function setLastLogin($userid);

	public function getByCredentials($useremail, $password);
}
