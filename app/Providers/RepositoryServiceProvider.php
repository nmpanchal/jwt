<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->bootRepositories();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function bootRepositories()
	   {
  		$this->app->bind('App\Repositories\UserRepositoryInterface', 'App\Repositories\UserRepository');
      $this->app->bind('App\Repositories\Clients\ClientUserRepositoryInterface', 'App\Repositories\Clients\ClientUserRepository');
      $this->app->bind('App\Repositories\Clients\ContentRepositoryInterface', 'App\Repositories\Clients\ContentRepositoryEloquent');
  	  $this->app->bind('App\Repositories\SettingRepositoryInterface', 'App\Repositories\SettingRepository');
    }

}
