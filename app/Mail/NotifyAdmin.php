<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyAdmin extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        $this->email = $details['email'];
        $this->preferName = $details['preferName'];
        $this->url = $details['url'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

      return $this->view('email.NotifyAdmin')
              ->subject("New client verified on platform")
              ->with('clientData',array('email'=>$this->email, 'name'=>$this->preferName, 'url' =>url('v1/verify?'.$this->url)));
    }
}
