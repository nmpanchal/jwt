<?php
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;

class UserPoa extends Model implements AuthenticatableContract {

    use Authenticatable;
    protected $table = 'users';
    protected $primaryKey  = 'id';
    public $timestamps = true;
}


 ?>
