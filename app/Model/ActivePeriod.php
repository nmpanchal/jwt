<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ActivePeriod extends Model
{
		public $timestamps = false;
		protected $connection = 'mysql';
		protected $table = 'ta_active_period';

		protected $fillable = ['time_to_expiry'];
}
