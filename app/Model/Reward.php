<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Reward extends Model
{
    public $timestamps = true;
    protected $dateFormat = 'U';
    protected $connection = 'mysql';
    protected $table = 'ta_rewards';
}
