<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FkCandidateSalary extends Model
{
  /**
   * All basic linking first for default columns, connection, table
   */
  public $timestamps = true;
  //protected $dateFormat = 'U';
  protected $connection = 'mysql';
  protected $table = 'ta_fk_candidate_salary_band';

  protected $fillable = [];

  /*reverse one to many relationship*/
  public function candidate(){
    return $this->belongsTo('App\Model\Candidate','candidate_id', 'id');
  }

  /**
  *TODO: Add candidate and salary key.
  *@author Yamin
  *@param $candidate_id,$salary_band_id
  *@return boolean
  **/
  public function addCandidateSalary($candidate_id,$salary_band_id){
      $fkCandidateSalary = new FkCandidateSalary();
      $fkCandidateSalary->candidate_id = $candidate_id;
      $fkCandidateSalary->salary_band_id = $salary_band_id;
      return $fkCandidateSalary->save();
  }

  /**
  *TODO: remove candidate and salary key
  *@author Yamin
  *@param $candidate_id
  *@return boolean
  **/
  
  public function removeCandiadteSalary($candidateId){
      $fkCandidateSalaryDelete = FkCandidateSalary::where('candidate_id', '=', $candidateId)->delete();
      return $fkCandidateSalaryDelete;
  }
}
