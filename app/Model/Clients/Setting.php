<?php

namespace App\Model\Clients;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Setting extends Model
{
    public $timestamps = true;
	protected $table = 'settings';
	protected $fillable = ['type','value'];
	use SoftDeletes;
    protected $dates = ['deleted_at'];


}
