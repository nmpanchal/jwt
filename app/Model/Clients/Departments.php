<?php

namespace App\Model\Clients;

use Illuminate\Database\Eloquent\Model;

class Departments extends Model
{
  /**
   * @var string
   * @desc VERY IMPORTANT for dynamic DB linking
   */
  protected $connection = 'tenantsql';

  protected $table = 'departments';

  protected $hidden = ['department_id', 'enabled'];

  protected $fillable = ['department_name', 'enabled'];
}
