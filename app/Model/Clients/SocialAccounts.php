<?php

namespace App\Model\Clients;

use Illuminate\Database\Eloquent\Model;

class SocialAccounts extends Model
{
  /**
   * @var string
   * @desc VERY IMPORTANT for dynamic DB linking
   */
  protected $connection = 'tenantsql';

  protected $table = 'social_accounts';

  protected $hidden = ['id','enabled'];

  protected $fillable = ['social_name', 'social_url', 'secret','enabled'];

}
