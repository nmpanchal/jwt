<?php

namespace App\Model\Clients;

use Vinkla\Hashids\Facades\Hashids;
use Illuminate\Database\Eloquent\Model;
use App\Model\Clients\ApprovalType;

class Content extends Model
{
    protected $connection = 'tenantsql';

    protected $table = 'content';

    protected $primaryKey = 'id';

    protected $visible =['*'];

    protected $fillable = ['user_id', 'department_id', 'title','description', 'image_url', 'appoval', 'approval_flag'];

    // relationship with ApprovalType
    public function approvalType(){
      return $this->belongsTo(ApprovalType::class, 'approval_flag', 'id');
    }

    public function getIdAttribute($value){
      return Hashids::encode($value);
    } 

    /*public function setIdAttribute($value){
    $this->attributes['id']=Hashids::decode($value);
  } */
}
