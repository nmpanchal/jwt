<?php

namespace App\Model\Clients;

//use App\Model\Clients\SocialAccounts;
use Illuminate\Database\Eloquent\Model;

class UserSocial extends Model
{
  /**
   * @var string
   * @desc VERY IMPORTANT for dynamic DB linking
   */
  protected $connection = 'tenantsql';

  protected $table = 'user_social';

  protected $hidden = ['id','user_id','social_accounts_id'];

  protected $fillable = ['user_id', 'social_accounts_id', 'user_secret','user_token'];


  public function socialAccountName()
    {
      return $this->belongsTo(SocialAccounts::class, 'id'); //->select(array('social_url', 'social_name'));
    }


}
