<?php

namespace App\Model\Clients;

use Illuminate\Database\Eloquent\Model;
 
class Tags extends Model
{
   		public $timestamps = true;
		protected $table = 'tags';
		protected $fillable = ['tag_name','user_id'];

		/*Define relation with user table*/
		public function user(){
  		  return $this->belongsTo('App\Model\Clients\ClientsUser','id', 'user_id');
  		}

  		

  		/**
  		*Add tags
  		*@author Yamin
  		*@param $tagName,$userId
  		*@return $tagId
  		**/
		public function addTag($tagName,$userId){
			$tags = new Tags();
			$tags->tag_name = $tagName;
			$tags->user_id = $userId;
			$tags->save();
			return $tags->id;
		}

		/**
  		*Edit tags
  		*@author Yamin
  		*@param $tagName,$userId,$id
  		*@return boolean
  		**/
		public function editTag($tagName,$userId,$id){
			$tags = Tags::find($id);
			if(!$tags){
				return false;
			}
			$tags->fill(['tag_name'=>$tagName,'user_id'=>$userId]);
			return $tags->save();
		}

		/**
  		*Remove tags
  		*@author Yamin
  		*@param $id
  		*@return boolean
  		*@link http://192.168.99.100:9000/api/v1/tags/delete?api_secret=9cXKYU4Wt8
  		{
			"id": 10
		}
  		**/
		public function deleteTag($id){
			$tags = Tags::find($id);
			if(!$tags){
				return false;
			}
			return $tags->delete();
		}

}
