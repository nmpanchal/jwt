<?php
namespace App\Model\Clients;
use App\Model\Clients\Departments;
use App\Model\Clients\UserSocial;
use Illuminate\Database\Eloquent\Model;


class TenantClientUsers extends Model {
		//use EntrustUserTrait;

		/**
		 * All basic linking first for default columns, connection, table
		 */
		public $timestamps = true;
		//protected $dateFormat = 'U';

    /**
     * @var string
     * @desc VERY IMPORTANT for dynamic DB linking
     */
    protected $connection = 'tenantsql';

		protected $table = 'users';

		protected $hidden = ['id', 'password', 'auth_token'];

		protected $fillable = ['name','password','initial_password', 'email','about_me','department','profileImage', 'auth_token','device_type','device_token'];

		protected $appends = ['profileImage'];

		public function department()
			{
			   return $this->belongsTo(Departments::class, 'department', 'department_id');
			}

		public function socialAccounts()
			{
					 return $this->hasMany(UserSocial::class,'user_id')->with('socialAccountName');;
			}

		public function getprofileImageAttribute($value){
		    if(null ===$value){
		      $value ="https://placeholdit.imgix.net/~text?txtsize=33&txt=TA&w=350&h=150";
		    }
		    return $value;
		  }

}
