<?php

namespace App\Model\Clients;

use App\Model\Clients\Category;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    	public $timestamps = true;
		protected $table = 'category';
		protected $fillable = ['category','user_id'];

		/*Define relation with user table*/
		public function user(){
  		  return $this->belongsTo('App\Model\Clients\ClientsUser','id', 'user_id');
  		}

  		/**
  		*Add category
  		*@author Yamin
  		*@param $categoryData
  		*@return $categoryId
  		**/
		public function addCategory($categoryData){
			$category = new Category();
			$category->category = $categoryData['category'];
			$category->user_id = $categoryData['user_id'];
			$category->save();
			return $category->id;
		}

		/**
  		*Edit category
  		*@author Yamin
  		*@param $categoryData
  		*@return boolean
  		**/
		public function editCategory($categoryData){
			$category = Category::find($categoryData['id']);
			if(!$category){
				return false;
			}
			$category->fill(['category'=>$categoryData['category'],'user_id'=>$categoryData['user_id']]);
			return $category->save();
		}

		/**
  		*Remove category
  		*@author Yamin
  		*@param $id
  		*@return boolean
  		
  		**/
		public function deleteCategory($id){
			$category = Category::find($id);
			if(!$category){
				return false;	
			}
			return $category->delete();
		}
}
