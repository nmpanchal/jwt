<?php

namespace App\Model\Clients;

use Illuminate\Database\Eloquent\Model;
use App\Model\Clients\Content;

class ApprovalType extends Model
{
    protected $connection = 'tenantsql';
    protected $table ='approval_type';
    protected $hidden = ['id'];

  public function content(){
    return $this->hasMany(Content::class, 'approval_flag');
  }


}
