<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ContractType extends Model
{
    // basic linking informations
    public $timestamps = true;
    protected $dateFormat = 'U';
    protected $connection = 'mysql';
    protected $table = 'ta_contracttype';

    protected $fillable = ['contract_type', 'description', 'client_id','is_active'];


    public function getAll(){
      return ContractType::where('is_active', 1)
                          ->where('client_id',0)->get();
    }

 

    public function getAllForClient($clientid){
      // as we have assumed all NULL clientIDs in contract type
      // means common for all clients
      // lets parse it with Logic
      $inClientList = array(0, $clientid);
       return $contracts = ContractType::where('is_active', 1)
                        ->whereIn('client_id',$inClientList)
                        ->get();

    }
    /*define a relation*/
    public function job(){
        return $this->belongsToMany('App\Model\Job', 'contract_type_job');
     }

}
