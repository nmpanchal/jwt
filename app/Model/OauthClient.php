<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OauthClient extends Model
{
    protected $table = 'oauth_clients';

    protected $fillable = ['user_id','name','id','secret','password_client','personal_access_client','redirect','revoked'];
}
