<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
   		 /**
		 * All basic linking first for default columns, connection, table
		 */
		public $timestamps = true;
		//protected $dateFormat = 'U';
		protected $connection = 'mysql';
		protected $table = 'ta_clients';

		/*One to many relation with user table*/
		public function user(){
		  return $this->hasMany('App\Model\User','client_id', 'id');
		}
}
