<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PublishStatus extends Model
{
		public $timestamps = false;
    	protected $connection = 'mysql';
		protected $table = 'ta_publish_status';

		protected $fillable = ['description','client_id'];
}
