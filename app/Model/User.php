<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class User extends Model {

		//use EntrustUserTrait;

		/**
		 * All basic linking first for default columns, connection, table
		 */
		public $timestamps = true;
		protected $dateFormat = 'U';
		protected $connection = 'mysql';
		protected $table = 'ta_users';

		protected $fillable = ['prefer_name','last_online','login_email','login_password',
		'api_secret','phone', 'job_title', 'activation_token','domain_name','database_name','slug'];


}
