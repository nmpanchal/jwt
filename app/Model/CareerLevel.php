<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CareerLevel extends Model
{
      /**
   		* All basic linking first for default columns, connection, table
   	  */
	  public $timestamps = true;
	  //protected $dateFormat = 'U';
	  protected $connection = 'mysql';
	  protected $table = 'ta_career_level';

	  protected $fillable = [];
	  /*One to one relation with candidate table*/
	  public function candidate(){
	    return $this->belongsTo('App\Model\Candidate','id', 'career_level_id');
	  }
}
