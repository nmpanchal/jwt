<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Sector extends Model
{
	   /**
	    * All basic linking first for default columns, connection, table
	   */
	     public $timestamps = true;
	     //protected $dateFormat = 'U';
	     protected $connection = 'mysql';
	     protected $table = 'ta_sector';
	     protected $fillable = [];

   		public function candidate(){
   			return $this->belongsTo('App\Model\Candidate','id', 'sector_id');
  		}
}
