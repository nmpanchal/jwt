<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class JobDetails extends Model
{
  /**
   * All basic linking first for default columns, connection, table
   */
  public $timestamps = true;
  protected $dateFormat = 'U';
  protected $connection = 'mysql';
  protected $table = 'ta_job_details';

  protected $fillable = [];
  /*define relations*/
  public function job(){
    return $this->belongsTo('App\Model\Job','job_id', 'id');
  }

  public function salaryBand(){
    return $this->belongsTo('App\Model\SalaryBand','salary_band_id', 'id');
  }

  public function sector(){
    return $this->belongsTo('App\Model\Sector','sector_id', 'id');
  }
  public function reward(){
    return $this->belongsTo('App\Model\Reward','reward_id', 'id');
  }
  public function country(){
    return $this->belongsTo('App\Model\Country','country_id', 'id');
  }
  public function client(){
    return $this->belongsTo('App\Model\Client','clients_id', 'id');
  }
  public function activePeriod(){
    return $this->belongsTo('App\Model\ActivePeriod','active_period_id', 'id');
  }
  public function publishStatus(){
    return $this->belongsTo('App\Model\PublishStatus','published_status', 'id');
  }

}
