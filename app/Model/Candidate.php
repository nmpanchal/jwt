<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
  public $timestamps = true;
  //protected $dateFormat = 'U';
  protected $connection = 'mysql';
  protected $table = 'ta_candidates';


  /*check filled with the latest field changes*/
  protected $fillable = ['first_name','last_name','email','honor_title','profile_pic','address1','address2','town','postcode','country_id','exp_details','education_details','notice_period','licence_available','availability','ready_to_relocate','status','bio_details','uk_passport','career_level_id','education_level_id','sector_id'];

  public function country(){
    return $this->hasOne('App\Model\Country','id', 'country_id');
  }
  public function sector(){
    return $this->hasOne('App\Model\Sector','id', 'sector_id');
  }
  /*One to one relation with ta_education_level table*/
  public function educationLevel(){
    return $this->hasOne('App\Model\EducationLevel','id', 'education_level_id');
  }
  /*One to one relation with ta_career_level table*/
  public function careerLevel(){
    return $this->hasOne('App\Model\CareerLevel','id', 'career_level_id');
  }
  /*One to many relation with ta_fk_candidate_contracttype table*/
  public function candidateContract(){
    return $this->hasMany('App\Model\FkCandidateContract','candidate_id', 'id');
  }
  /*One to many relation with ta_fk_candidate_salary_band table*/
  public function candidateSalary(){
    return $this->hasMany('App\Model\FkCandidateSalary','candidate_id', 'id');
  }

 
}
