<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class JobTag extends Model
{
    public $timestamps = true;
    protected $dateFormat = 'U';
    protected $connection = 'mysql';
    protected $table = 'ta_job_tags';

	  
}
