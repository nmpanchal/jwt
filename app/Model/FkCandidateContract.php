<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FkCandidateContract extends Model
{
   /**
   * All basic linking first for default columns, connection, table
   */
  public $timestamps = true;
  //protected $dateFormat = 'U';
  protected $connection = 'mysql';
  protected $table = 'ta_fk_candidate_contracttype';

  protected $fillable = [];
  /*one to many relation*/
  public function candidate(){
    return $this->belongsTo('App\Model\Candidate','candidate_id', 'id');
  }

  /**
  *TODO: Add candidate and contract type key.
  *@author Yamin
  *@param $candidate_id,$contracttype_id
  *@return boolean
  **/
  public function addCandidateContractType($candidate_id,$contracttype_id){
      $fkCandidateContract = new FkCandidateContract();
      $fkCandidateContract->candidate_id = $candidate_id;
      $fkCandidateContract->contracttype_id = $contracttype_id;
      return $fkCandidateContract->save();
  }

  /**
  *TODO: remove candidate and contract key.
  *@author Yamin
  *@param $candidate_id
  *@return boolean
  **/
  
  public function removeCandiadteContract($candidateId){
      $fkCandidateCandidate = FkCandidateContract::where('candidate_id', '=', $candidateId)->delete();
      return $fkCandidateCandidate;
  }

  
}

