<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
  /**
   * All basic linking first for default columns, connection, table
   */
  public $timestamps = true;
  protected $dateFormat = 'U';
  protected $connection = 'mysql';
  protected $table = 'ta_jobs'; 

  protected $fillable = [];
  /*Define realtions*/
  public function jobdetails(){
    return $this->hasOne('App\Model\JobDetails','job_id', 'id');
  }
  
  /*public function salary(){
   return $this->hasOne('App\Model\JobDetails')->SalaryBand();
  }*/  
  

  public function contractType(){
          return $this->belongsToMany('App\Model\ContractType');
  }

  public function tags(){
          return $this->belongsToMany('App\Model\Tags');
  }

  public function taggedUsers(){
          return $this->belongsToMany('App\Model\User')->withPivot('role_name');
  }
}
