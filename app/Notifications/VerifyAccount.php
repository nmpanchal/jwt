<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\User;

class VerifyAccount extends Notification
{
    use Queueable;
    protected $user;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
      $token = $this->user->activation_token;
      $send_url = 'e='.base64_encode($this->user->email).'&t='.base64_encode($token);
      return (new MailMessage)
                  ->subject('TalentAdvocate : Thanks for signing up with us.')
                  ->success()
                  ->line('Welcome, '.$this->user->prefer_name.' !')
                  ->action('Yes i signed up, confirm it !', url('/verify?'.$send_url))
                  ->line('TalentAdvocate builds your employer brand from the inside out by giving your employees a safe environment to create and promote company content across personal and corporate social channels.');

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
