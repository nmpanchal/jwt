<?php

use App\User;
use App\Notifications\VerifyEmail;
use App\Notifications\ClientSignedup;
use App\Notifications\VerifyAccount;
use App\Events\ClientSignedup as EvSignedup;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
  // Creating a token without scopes...
  //$token = $user->createToken('teest')->accessToken;

  Route::get('/welcome', function () {
      return "yes its api route and up";
  });


  Route::get('/notify', function(){
    //dd(Config::get('mail'));
    $client = App\User::find(1);
    //$client->notify(new VerifyAccount($client));
    Event::fire(new EvSignedup($client));
  });


  /*
  | @mention Ravi
  | Following routes will be under group named as "client" as they are
  | specific to clients only
  |
  */

  Route::group(['prefix' => 'v1/'], function(){
    // Generic groups TA level
    Route::get('test', function(){ echo "product route"; });
    Route::post('signup', 'UserController@create'); //No verify in API
    Route::get('verify', 'UserController@verify');


    // Client groups
    Route::group(['prefix' => 'clients/', 'middleware'=> 'database'], function(){


      Route::post('/issueToken','Auth\CustomAuthController@issueToken');
      Route::get('test', function(){ echo "client route called"; });
      Route::post('/register-user', 'ClientController@createClientUser');

      Route::post('login', 'CustomLoginController@Login');

      //route for content as its client specific
      Route::get('content', [
        'as' => 'content',
        'uses'=> 'Clients\ContentController@index'
      ]);

      Route::get('content/approved', 'Clients\ContentController@getAllApproved');
      Route::get('content/unapproved', 'Clients\ContentController@getAllNotApproved');
      Route::post('content', 'Clients\ContentController@createContent');
      Route::put('content/approve', 'Clients\ContentController@approveContent');

    }); // end of GROUP clients
  }); //end of GROUP V1

  Route::resource('client','ClientController');

  Route::get('token', function(){
    $user = App\User::find(10);
    $vr = $user->createToken('testing')->accessToken;
    dd($vr);
  });
