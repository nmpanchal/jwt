<?php

use App\User;
use App\Events\ClientVerified;
use App\Notifications\ClientSignedup;
//Auth::attempt($user);

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/notify', function(){
  //dd(Config::get('mail'));
  $user = App\User::find(42);
  $user->notify(new ClientSignedup($user));
  Event::fire(new ClientVerified($user));
});

/*
| @mention Ravi
| Following routes will be under group named as "client" as they are
| specific to clients only
|
*/

Route::group(['prefix' => 'v1'], function(){
  //Verify in WEB actually as we need settings to be added
  Route::get('verify', 'UserController@verify');
}); //end of GROUP V1

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::group(['prefix' => 'cms/', 'middleware'=> 'cms'], function(){
  Route::get('{id}/careers', 'CmsController@setHome');
  Route::get('{id}/contact', 'CmsController@contact');
});
