<!DOCTYPE html>
<html>
    <head>
        <title>TalentBake CRM API</title>


        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title"><img src="//talentbake.com/img/logo.png" /></div>
            </div>
        </div>
    </body>
</html>
