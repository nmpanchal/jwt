<?php

/*$factory('App\Model\Clients\ClientsUser',[
  'id' => $faker->unique()->numberBetween(8,28),
  'name' => $faker->name,
  'email' => $faker->unique()->email,
  'password' => bcrypt('test123'),
  'about_me'  => $faker->sentence,
  'department' => $faker->numberBetween(0,1),
  'active' =>$faker->numberBetween(0,1)
  ]); */

$factory('App\Model\Clients\Content',[
  'user_id' => $faker->numberBetween(1,2), //'factory:App\Model\Clients\ClientsUser',
  'title' => $faker->sentence,
  'description' => $faker->paragraph,
  'image_url' => $faker->imageUrl($width = 640, $height = 480),
  'approval' =>$faker->numberBetween(0,1),
  'approval_flag' =>$faker->numberBetween(1,2),
  'department_id' =>$faker->numberBetween(1,2), //'factory:App\Model\Clients\Department
  'created_at' => $faker->unixTime($max = 'now'),
  'updated_at' =>$faker->unixTime($max = 'now')
  ]);
