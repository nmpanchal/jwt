FROM centos:centos7
MAINTAINER Ravi Tiwari <ravi@isensical.com>

ENV code_root /code
ENV httpd_conf ${code_root}/env/httpd.conf

RUN rpm -ivh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
RUN rpm -ivh http://rpms.famillecollet.com/enterprise/remi-release-7.rpm
RUN yum install -y yum-utils
RUN yum -y install initscripts && yum clean all
RUN yum -y install httpd
RUN yum install -y sendmail
# Run mail listener
RUN sendmail -bd
CMD sleep infinity

RUN yum install --enablerepo=epel,remi-php70,remi -y \
                              php70-php.x86_64 \
                              php70-php-bcmath.x86_64 \
                              php70-php-cli.x86_64 \
                              php70-php-common.x86_64 \
                              php70-php-gd.x86_64 \
                              php70-php-mbstring.x86_64 \
                              php70-php-mcrypt.x86_64 \
                              php70-php-mysqlnd.x86_64 \
                              php70-php-pdo.x86_64 \
                              php70-php-xml.x86_64 \
                              mysql



RUN ln -s /usr/bin/php70 /usr/bin/php \
    && ln -s /etc/opt/remi/php70/php.ini /etc/php.ini \
    && ln -s /etc/opt/remi/php70/php.d /etc/php.d

RUN sed -i -e "s|^;date.timezone =.*$|date.timezone = UTC|" /etc/php.ini
RUN sed -i -e "s|^;bcmath.scale  =.*$|bcmath.scale = 0|" /etc/php.ini
RUN rm /etc/httpd/conf.d/welcome.conf \
    && sed -i -e "s/Options\ Indexes\ FollowSymLinks/Options\ -Indexes\ +FollowSymLinks/g" /etc/httpd/conf/httpd.conf \
    && sed -i "s/\/var\/www\/html/\/var\/www/g" /etc/httpd/conf/httpd.conf \
    && echo "FileETag None" >> /etc/httpd/conf/httpd.conf \
    && sed -i -e "s/expose_php\ =\ On/expose_php\ =\ Off/g" /etc/php.ini \
    && sed -i -e "s/\;error_log\ =\ php_errors\.log/error_log\ =\ \/var\/log\/php_errors\.log/g" /etc/php.ini \
    && echo "ServerTokens Prod" >> /etc/httpd/conf/httpd.conf \
    && echo "ServerSignature Off" >> /etc/httpd/conf/httpd.conf
RUN echo -e "$(hostname -i)\t$(hostname) $(hostname).localhost" >> /etc/hosts
ADD . $code_root
WORKDIR /code
RUN test -e $httpd_conf && echo "Include $httpd_conf" >> /etc/httpd/conf/httpd.conf
RUN usermod -u 1000 apache

EXPOSE 80
CMD ["/usr/sbin/httpd", "-D", "FOREGROUND"]

#EXPOSE 2525
